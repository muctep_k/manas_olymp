from django.shortcuts import get_object_or_404
from rest_framework import generics
from rest_framework.permissions import AllowAny

from api.models import User
from api.serializers import UserProfileSerializer, EditUserProfileSerializer, UserSubmissionSerializer, \
    UserRankingSerializer
from api.views.app import StandardResultsSetPagination


class UserProfileView(generics.RetrieveAPIView,
                      generics.UpdateAPIView):

    def get_object(self):
        user_id = self.request.GET.get('user_id')
        if user_id:
            return get_object_or_404(User, id=user_id)
        return self.request.user

    def get_serializer_class(self):
        if self.request.method == 'PATCH':
            return EditUserProfileSerializer
        else:
            return UserProfileSerializer


class UserSubmissionView(generics.ListAPIView):
    serializer_class = UserSubmissionSerializer

    def get_queryset(self):
        user_id = self.request.GET.get('user_id')
        if user_id:
            user = get_object_or_404(User, id=user_id)
        else:
            user = self.request.user
        return user.submissions.prefetch_related('problem', 'language').all().order_by('-submitted_at')[:50]


class UserRankView(generics.ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = UserRankingSerializer
    pagination_class = StandardResultsSetPagination

    def get_queryset(self):
        return User.objects.filter(is_staff=False).order_by('rank')


