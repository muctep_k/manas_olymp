from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from api.schemas import ErrorResponseAutoSchema, RefreshTokenSchema, OKSchema, SuccessfullLoginSchema
from api.views import LoginView, RegisterView, CustomTokenRefreshView, ListTagsView, ListProblemView, DetailProblemView, \
    SubmitSolutionView, ListLanguageView, DetailSubmissionView, ListSubmissionView, UserProfileView, UserSubmissionView, \
    UserRankView


class DocumentedLoginView(LoginView):

    @swagger_auto_schema(auto_schema=ErrorResponseAutoSchema,
                         responses={"200": SuccessfullLoginSchema},
                         tags=["Auth Endpoints"],
                         operation_id="Login Endpoint",
                         operation_description="Get the access and refresh token for the entered user.")
    def post(self, request, *args, **kwargs):
        return super(DocumentedLoginView, self).post(request, *args, **kwargs)


class DocumentedRegisterView(RegisterView):

    @swagger_auto_schema(auto_schema=ErrorResponseAutoSchema,
                         responses={"200": OKSchema},
                         tags=["Auth Endpoints"],
                         operation_id="Register Endpoint",
                         operation_description="Register new user.")
    def post(self, request, *args, **kwargs):
        return super(DocumentedRegisterView, self).post(request, *args, **kwargs)

    @swagger_auto_schema(auto_schema=ErrorResponseAutoSchema,
                         responses={"200": OKSchema},
                         tags=["Auth Endpoints"],
                         operation_id="Confirm Register Endpoint",
                         operation_description="Confirms the token so the account of user will be activated.",
                         manual_parameters=[openapi.Parameter("token", openapi.IN_QUERY,type=openapi.TYPE_STRING,
                                                              description="Token", required=True)])
    def get(self, request, *args, **kwargs):
        return super(DocumentedRegisterView, self).get(request, *args, **kwargs)


class DocumentedCustomTokenRefreshView(CustomTokenRefreshView):

    @swagger_auto_schema(auto_schema=ErrorResponseAutoSchema,
                         responses={"200": RefreshTokenSchema},
                         tags=["Auth Endpoints"],
                         operation_id="Refresh Token Endpoint",
                         operation_description="Get new access token if expired by refresh token")
    def post(self, request, *args, **kwargs):
        return super(DocumentedCustomTokenRefreshView, self).post(request, *args, **kwargs)


class DocumentedListTagsView(ListTagsView):

    @swagger_auto_schema(auto_schema=ErrorResponseAutoSchema,
                         tags=["Problems Endpoints"],
                         operation_id="Get Tags endpoint",
                         operation_description="Get list of all available tags")
    def get(self, request, *args, **kwargs):
        return super(DocumentedListTagsView, self).get(request, *args, **kwargs)


class DocumentedListProblemView(ListProblemView):
    @swagger_auto_schema(auto_schema=ErrorResponseAutoSchema,
                         tags=["Problems Endpoints"],
                         operation_id="Get Problems endpoint",
                         operation_description="Get list of all problems",
                         manual_parameters=[openapi.Parameter("tag_id", openapi.IN_QUERY, type=openapi.TYPE_INTEGER, required=False),
                                            openapi.Parameter("complexity", openapi.IN_QUERY, type=openapi.TYPE_STRING, required=False)])
    def get(self, request, *args, **kwargs):
        return super(DocumentedListProblemView, self).get(request, *args, **kwargs)


class DocumentedDetailProblemView(DetailProblemView):
    @swagger_auto_schema(auto_schema=ErrorResponseAutoSchema,
                         tags=["Problems Endpoints"],
                         operation_id="Get Problem endpoint",
                         operation_description="Get detailed information of problem by id",
                         )
    def get(self, request, *args, **kwargs):
        return super(DocumentedDetailProblemView, self).get(request, *args, **kwargs)


class DocumentedSubmitSolutionView(SubmitSolutionView):
    @swagger_auto_schema(auto_schema=ErrorResponseAutoSchema,
                         responses={"200": RefreshTokenSchema},
                         tags=["Problems Endpoints"],
                         operation_id="Submit solution Endpoint",
                         operation_description="Submit user solution to the problem. Authorization is required.")
    def post(self, request, *args, **kwargs):
        return super(DocumentedSubmitSolutionView, self).post(request, *args, **kwargs)


class DocumentedListLanguageView(ListLanguageView):
    @swagger_auto_schema(auto_schema=ErrorResponseAutoSchema,
                         responses={"200": RefreshTokenSchema},
                         tags=["Problems Endpoints"],
                         operation_id="List programming languages Endpoint",
                         operation_description="List available programming languages")
    def get(self, request, *args, **kwargs):
        return super(DocumentedListLanguageView, self).get(request, *args, **kwargs)


class DocumentedDetailSubmissionView(DetailSubmissionView):

    @swagger_auto_schema(auto_schema=ErrorResponseAutoSchema,
                         tags=["Problems Endpoints"],
                         operation_id="Get Submission endpoint",
                         operation_description="Return detailed information about submission.",
                         )
    def get(self, request, *args, **kwargs):
        return super(DocumentedDetailSubmissionView, self).get(request, *args, **kwargs)


class DocumentedListSubmissionView(ListSubmissionView):

    @swagger_auto_schema(auto_schema=ErrorResponseAutoSchema,
                         tags=["Problems Endpoints"],
                         operation_id="Get Submissions list endpoint",
                         operation_description="Return queue of submissions",
                         )
    def get(self, request, *args, **kwargs):
        return super(DocumentedListSubmissionView, self).get(request, *args, **kwargs)


class DocumentedUserProfileView(UserProfileView):

    @swagger_auto_schema(auto_schema=ErrorResponseAutoSchema,
                         tags=["Profile Endpoints"],
                         operation_id="Get User Info endpoint",
                         operation_description="Return the info about user. If user_id is not specified, the info"
                                               " will be about current authorized user.",
                         manual_parameters=[
                             openapi.Parameter("user_id", openapi.IN_QUERY, type=openapi.TYPE_INTEGER, required=False)]
                         )
    def get(self, request, *args, **kwargs):
        return super(DocumentedUserProfileView, self).get(request, *args, **kwargs)

    @swagger_auto_schema(auto_schema=None)
    def put(self, request, *args, **kwargs):
        return super(DocumentedUserProfileView, self).put(request, *args, **kwargs)

    @swagger_auto_schema(auto_schema=ErrorResponseAutoSchema,
                         tags=["Profile Endpoints"],
                         operation_id="Update user info endpoint",
                         operation_description="Update currents user information or password.")
    def patch(self, request, *args, **kwargs):
        return super(DocumentedUserProfileView, self).patch(request, *args, **kwargs)


class DocumentedUserSubmissionView(UserSubmissionView):

    @swagger_auto_schema(auto_schema=ErrorResponseAutoSchema,
                         tags=["Profile Endpoints"],
                         operation_id="List User Submissions Endpoint",
                         operation_description="Return the submissions of user. If user_id is not specified, the info"
                                               " will be about current authorized user.")
    def get(self, request, *args, **kwargs):
        return super(DocumentedUserSubmissionView, self).get(request, *args, **kwargs)


class DocumentedUserRankView(UserRankView):

    @swagger_auto_schema(auto_schema=ErrorResponseAutoSchema,
                         tags=["Dashboard Endpoints"],
                         operation_id="User Ranks Endpoint",
                         operation_description="List Users and their ranks")
    def get(self, request, *args, **kwargs):
        return super(DocumentedUserRankView, self).get(request, *args, **kwargs)

