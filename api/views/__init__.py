from .auth import LoginView, RegisterView, CustomTokenRefreshView
from .app import ListProblemView, ListTagsView, DetailProblemView, SubmitSolutionView, ListLanguageView, \
    DetailSubmissionView, ListSubmissionView
from .user import UserProfileView, UserSubmissionView, UserRankView
from .documented import DocumentedLoginView, DocumentedRegisterView, DocumentedCustomTokenRefreshView, \
    DocumentedListTagsView, DocumentedListProblemView, DocumentedDetailProblemView, DocumentedSubmitSolutionView, \
    DocumentedListLanguageView, DocumentedDetailSubmissionView, DocumentedListSubmissionView, DocumentedUserProfileView, \
    DocumentedUserSubmissionView, DocumentedUserRankView
