from django.db.models import Prefetch
from django.shortcuts import get_object_or_404
from rest_framework import generics, status
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from api.models import Problem, Tags, Submission
from api.models.app import ProgrammingLanguage, PROCESSING_SUBMISSION_STATUS
from api.serializers import ListProblemSerializer, ListTagsSerializer, DetailProblemSerializer, \
    CreateSubmissionSerializer, ListLanguageSerializer, DetailSubmissionSerializer, SubmissionQueueSerializer


class ListTagsView(generics.ListAPIView):
    permission_classes = [AllowAny]
    serializer_class = ListTagsSerializer

    def get_queryset(self):
        return Tags.objects.all().order_by('title')


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 50
    page_size_query_param = 'page_size'
    max_page_size = 1000


class ListProblemView(generics.ListAPIView):
    pagination_class = StandardResultsSetPagination
    permission_classes = [AllowAny]
    serializer_class = ListProblemSerializer

    def get_queryset(self):
        tag_id, complexity = self.request.GET.get('tag_id'), self.request.GET.get('complexity')
        if self.request.user.is_authenticated:
            qs = Problem.objects.all().order_by('id').distinct().\
                prefetch_related(Prefetch('submissions', queryset=Submission.objects.filter(user=self.request.user)),
                                 'tags')
        else:
            qs = Problem.objects.all().order_by('id').distinct().prefetch_related('tags')
        if tag_id:
            qs = qs.filter(tags__in=tag_id).distinct()
        if complexity:
            qs = qs.filter(complexity=complexity).distinct()
        return qs


class DetailProblemView(generics.RetrieveAPIView):
    permission_classes = [AllowAny]
    lookup_field = 'id'
    serializer_class = DetailProblemSerializer
    queryset = Problem.objects.all()


class SubmitSolutionView(generics.GenericAPIView):
    serializer_class = CreateSubmissionSerializer

    def post(self, request, *args, **kwargs):
        problem = get_object_or_404(Problem, id=self.kwargs.get('problem_id'))
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request,
                                                    'object': problem})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        self.request.user.sent_submissions = self.request.user.sent_submissions + 1
        self.request.user.save()
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class DetailSubmissionView(generics.RetrieveAPIView):
    serializer_class = DetailSubmissionSerializer

    def get_object(self):
        return Submission.objects.select_related('problem', 'language', 'user')\
            .prefetch_related('testsubmission_set').get(id=self.kwargs.get('id'))


class ListLanguageView(generics.ListAPIView):
    serializer_class = ListLanguageSerializer
    queryset = ProgrammingLanguage.objects.all().order_by('name')
    permission_classes = [AllowAny]


class ListSubmissionView(generics.ListAPIView):
    serializer_class = SubmissionQueueSerializer
    pagination_class = StandardResultsSetPagination
    permission_classes = [AllowAny]

    def get_queryset(self):
        return Submission.objects.select_related('user', 'problem', 'language').all().order_by('-submitted_at')


class CallbackView(generics.GenericAPIView):
    permission_classes = [AllowAny]
    serializer_class = None

    def put(self, request, *args, **kwargs):

        data = request.data
        from api.models import TestSubmission
        import base64
        test_submission = TestSubmission.objects.select_related('submission', 'submission__user').get(judge_token=data['token'])
        submission = test_submission.submission
        test_submission.time_taken = data['time']
        test_submission.memory_taken = data['memory']
        if data['stdout']:
            test_submission.output = base64.b64decode(data['stdout'])
        test_submission.status = data['status']['description']
        test_submission.save()
        # if no test cases to test left
        if not submission.testsubmission_set.filter(status=PROCESSING_SUBMISSION_STATUS).exists():
            submission = Submission.objects.prefetch_related('testsubmission_set').get(id=submission.id)
            submission.status = submission.final_status
            submission.time_taken = submission.final_time_taken
            submission.memory_taken = submission.final_memory_taken
            user = test_submission.submission.user
            user.solved_problems = user.solved_problems + 1
            user.save()
            submission.save()
        return Response(data={}, status=status.HTTP_200_OK)

