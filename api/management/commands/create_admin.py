from django.core.management.base import BaseCommand
from django.utils import timezone

from api.models import User, Problem


class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        user = User.objects.get_or_create(email='admin@mail.com')[0]
        user.set_password('8614223048465')
        user.is_superuser = True
        user.is_active = True
        user.is_staff = True
        user.save()
