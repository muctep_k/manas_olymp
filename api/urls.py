from django.urls import path, re_path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions
from rest_framework.routers import DefaultRouter

from api.views import DocumentedLoginView, DocumentedRegisterView, DocumentedCustomTokenRefreshView, \
    DocumentedListTagsView, DocumentedListProblemView, DocumentedDetailProblemView, DocumentedSubmitSolutionView, \
    DocumentedListLanguageView, DocumentedDetailSubmissionView, DocumentedListSubmissionView, DocumentedUserProfileView, \
    DocumentedUserSubmissionView, DocumentedUserRankView
from api.views.app import CallbackView

schema_view = get_schema_view(
    openapi.Info(
        title="Manas Olymp Backend",
        default_version='v1',
        description="Backend for the Manas Olympiad system",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="contact@snippets.local"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,)
)
app_name = 'api'

__all__ = ['urlpatterns']

urlpatterns = [
    re_path(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0),
            name='swagger'),
    path('login/', DocumentedLoginView.as_view(), name='login'),
    path('refresh_token/', DocumentedCustomTokenRefreshView.as_view(), name='refresh_token'),
    path('register/', DocumentedRegisterView.as_view(), name='register'),
    path('tags/', DocumentedListTagsView.as_view(), name='list_tags'),
    path('problems/', DocumentedListProblemView.as_view(), name='list_problems'),
    path('problem/<int:id>/', DocumentedDetailProblemView.as_view(), name='detail_problem'),
    path('create_submission/<int:problem_id>/', DocumentedSubmitSolutionView.as_view(), name='submit_problem'),
    path('submission/<int:id>/', DocumentedDetailSubmissionView.as_view(), name='detail_submission'),
    path('submissions/', DocumentedListSubmissionView.as_view(), name='list_submission'),
    path('callback/', CallbackView.as_view(), name='callback'),
    path('languages/', DocumentedListLanguageView.as_view(), name='list_programming_languages'),
    path('users/profile/', DocumentedUserProfileView.as_view(), name='user_profile'),
    path('users/submissions/', DocumentedUserSubmissionView.as_view(), name='user_submissions'),
    path('leaderboard/', DocumentedUserRankView.as_view(), name='user_leaderboard'),

]

router = DefaultRouter(trailing_slash=False)

urlpatterns += router.urls
