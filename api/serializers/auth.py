from django.db import transaction
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework_simplejwt.serializers import TokenRefreshSerializer
from rest_framework_simplejwt.tokens import RefreshToken

from api.models import User
from api.tasks import send_email_verification
from api.utils import VerificationToken


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('photo',
                  'email',
                  'is_staff')


class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True, allow_null=False)
    password = serializers.CharField(required=True, allow_null=False)

    def validate(self, attrs):
        attrs = super(LoginSerializer, self).validate(attrs)
        email, password = attrs['email'].lower(), attrs['password']
        try:
            user = User.objects.get(email=email)
            if not user.check_password(password):
                raise ValidationError(detail={"password": ["Incorrect password"]})
            print(user.is_active)
            if not user.is_active:
                raise ValidationError(detail={"email": ["Your account is not verified yet!. Please, click the link in the email we sent you"]})
        except User.DoesNotExist:
            raise ValidationError(detail={"email": ["User not found"]})
        refresh = RefreshToken.for_user(user=user)
        user_data = UserSerializer(instance=user).data
        data = {'refresh': str(refresh),
                'access': str(refresh.access_token),
                'user': user_data}
        return data


class RegisterSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)
    first_name = serializers.CharField(max_length=30, required=True)
    last_name = serializers.CharField(max_length=150, required=True)
    password = serializers.CharField(required=True, trim_whitespace=False)
    confirm_password = serializers.CharField(required=True, trim_whitespace=False)

    def validate(self, attrs):
        password, confirm_password = attrs['password'], attrs['confirm_password']
        if password != confirm_password:
            raise ValidationError({"confirm_password": "Passwords should match!"})
        return attrs

    def validate_email(self, email):
        try:
            User.objects.get(email=email.lower())
            raise ValidationError(detail="Email already exists")
        except User.DoesNotExist:
            pass
        return email.lower()

    @transaction.atomic
    def save(self, **kwargs):
        email, password = self.validated_data['email'].lower(), self.validated_data['password']
        user = User.objects.create(email=email, password=password, first_name=self.validated_data['first_name'], last_name=self.validated_data['last_name'])
        user.set_password(password)
        user.save()
        token = VerificationToken().generate_token(user_id=user.id)
        send_email_verification.send(verification_token=token, user_email=user.email)


class CustomTokenRefreshSerializer(TokenRefreshSerializer):

    def validate(self, attrs):
        data = super(CustomTokenRefreshSerializer, self).validate(attrs)
        refresh = RefreshToken(attrs['refresh'])
        u = User.objects.get(id=refresh.get('user_id'))
        data.update({"user": UserSerializer(instance=u).data})
        return data
