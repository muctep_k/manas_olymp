from typing import Union

from django.db import transaction
from drf_yasg.utils import swagger_serializer_method
from rest_framework import serializers
from rest_framework.fields import CurrentUserDefault

from api.models import Problem, Tags, Submission, User
from api.models.app import ACCEPTED_SUBMISSION_STATUS, ProgrammingLanguage, TestSubmission
from api.tasks import send_submission_to_judge


class ListTagsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tags
        fields = ('id',
                  'title')


class ObjectInContext:
    requires_context = True

    def __call__(self, serializer_field):
        return serializer_field.context['object']

    def __repr__(self):
        return '%s()' % self.__class__.__name__


class ListProblemSerializer(serializers.ModelSerializer):
    tags = serializers.SlugRelatedField(many=True, slug_field='title', queryset=Tags.objects.all())
    status = serializers.SerializerMethodField()
    complexity = serializers.CharField(source='get_complexity_display')

    def get_status(self, instance: Problem) -> Union[bool, None]:
        user = self.context['request'].user
        if user.is_authenticated:
            submission = instance.submissions.all()
            if submission:
                submission.filter(status=ACCEPTED_SUBMISSION_STATUS)
                if submission:
                    return True  # Solved already
                else:
                    return False  # Tried but did not solved
            else:
                return None  # Did not try any time
        else:
            return None  # User is anonymous

    class Meta:
        model = Problem
        fields = ('id',
                  'title',
                  'all_submissions',
                  'successful_submissions',
                  'tags',
                  'complexity',
                  'status')


class ListSubmissionSerializer(serializers.ModelSerializer):
    submitted_at = serializers.DateTimeField(format="%d-%m-%Y %H:%M", read_only=True)

    class Meta:
        model = Submission
        fields = ('id',
                  'status',
                  'language',
                  'time_taken',
                  'memory_taken',
                  'submitted_at')


class ListLanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProgrammingLanguage
        fields = ('id',
                  'name')


class DetailProblemSerializer(serializers.ModelSerializer):
    tags = serializers.SlugRelatedField(many=True, slug_field='title', queryset=Tags.objects.all())
    submissions = serializers.SerializerMethodField()

    @swagger_serializer_method(serializer_or_field=ListSubmissionSerializer(many=True))
    def get_submissions(self, instance):
        user = self.context['request'].user
        if user.is_authenticated:
            return ListSubmissionSerializer(instance=user.submissions.all().order_by('-submitted_at')[:20],
                                            many=True).data
        return []

    class Meta:
        model = Problem
        fields = ('id',
                  'title',
                  'description',
                  'time_limit',
                  'memory_limit',
                  'input_example',
                  'output_example',
                  'tags',
                  'submissions')


class CreateSubmissionSerializer(serializers.ModelSerializer):
    id = serializers.ReadOnlyField()
    user = serializers.HiddenField(default=CurrentUserDefault(), write_only=True)
    problem = serializers.HiddenField(default=ObjectInContext(), write_only=True)
    code = serializers.CharField(write_only=True, max_length=4096)
    submitted_at = serializers.DateTimeField(format="%d-%m-%Y %H:%M", read_only=True)

    class Meta:
        model = Submission
        fields = ('id',
                  'user',
                  'problem',
                  'code',
                  'language',
                  'status',
                  'time_taken',
                  'memory_taken',
                  'submitted_at')

    @transaction.atomic
    def save(self, **kwargs):
        submission = super(CreateSubmissionSerializer, self).save(**kwargs)
        problem = self.validated_data['problem']
        test_submission = []
        print(problem.test_cases.all())
        for test_case in problem.test_cases.all():
            test_submission.append(TestSubmission(test_case_id=test_case.id, submission_id=submission.id))
        print(test_submission)
        TestSubmission.objects.bulk_create(test_submission)
        send_submission_to_judge.send(submission_id=submission.id)
        return submission


class TestCaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestSubmission
        fields = ('status',
                  'time_taken',
                  'memory_taken')


class SimpleProblemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Problem
        fields = ('id',
                  'title')


class SubmissionProblemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Problem
        fields = ('id',
                  'title',
                  'time_limit',
                  'memory_limit')


class SimpleUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id',
                  'first_name',
                  'last_name')


class DetailSubmissionSerializer(serializers.ModelSerializer):
    test_cases = serializers.SerializerMethodField()
    problem = serializers.SerializerMethodField()
    submitted_at = serializers.DateTimeField(format="%d-%m-%Y %H:%M", read_only=True)
    language = serializers.SlugRelatedField(slug_field='name', queryset=ProgrammingLanguage.objects.all())
    user = serializers.SerializerMethodField()

    @swagger_serializer_method(serializer_or_field=SubmissionProblemSerializer)
    def get_problem(self, instance: Submission):
        return SubmissionProblemSerializer(instance=instance.problem).data

    @swagger_serializer_method(serializer_or_field=SimpleUserSerializer)
    def get_user(self, instance: Submission):
        return SimpleUserSerializer(instance=instance.user).data

    @swagger_serializer_method(serializer_or_field=TestCaseSerializer)
    def get_test_cases(self, instance: Submission):
        return TestCaseSerializer(instance=instance.testsubmission_set.all(), many=True).data

    class Meta:
        model = Submission
        fields = ('problem',
                  'submitted_at',
                  'language',
                  'user',
                  'status',
                  'time_taken',
                  'memory_taken',
                  'test_cases',
                  'code')


class SubmissionQueueSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField()
    problem = serializers.SerializerMethodField()
    language = serializers.StringRelatedField()
    submitted_at = serializers.DateTimeField(format="%d-%m-%Y %H:%M", read_only=True)

    @swagger_serializer_method(serializer_or_field=SimpleUserSerializer)
    def get_user(self, instance: Submission):
        return SimpleUserSerializer(instance=instance.user).data

    @swagger_serializer_method(serializer_or_field=SimpleProblemSerializer)
    def get_problem(self, instance: Submission):
        return SimpleProblemSerializer(instance=instance.problem).data

    class Meta:
        model = Submission
        fields = ('id',
                  'user',
                  'problem',
                  'language',
                  'submitted_at',
                  'status')