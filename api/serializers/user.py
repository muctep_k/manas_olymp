from drf_extra_fields.fields import Base64ImageField
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from api.models import User, Submission
from api.serializers import DetailSubmissionSerializer
from api.serializers.app import ListSubmissionSerializer, SubmissionQueueSerializer


class UserProfileSerializer(serializers.ModelSerializer):
    date_joined = serializers.DateTimeField(format="%d/%m/%y %H:%M:%S")

    class Meta:
        model = User
        fields = ('full_name',
                  'email',
                  'photo',
                  'last_activity',
                  'location',
                  'date_joined',
                  'rank',
                  'solved_problems',
                  'sent_submissions',
                  'accepted_submissions')


class EditUserProfileSerializer(serializers.ModelSerializer):
    confirm_password = serializers.CharField(max_length=256, write_only=True)
    password = serializers.CharField(max_length=256, write_only=True)
    photo = Base64ImageField()

    def validate(self, attrs):
        password, confirm_password = attrs.get('password', None), attrs.get('confirm_password', None)
        if password != confirm_password:
            raise ValidationError(detail={'password': 'Password and confirm password must match!'})
        return attrs

    def save(self, **kwargs):
        confirm_password = self.validated_data.pop('password', None)
        if confirm_password:
            self.instance.set_password(confirm_password)
        super(EditUserProfileSerializer, self).save(**kwargs)

    class Meta:
        model = User
        fields = ('full_name',
                  'photo',
                  'location',
                  'password',
                  'confirm_password')


class UserSubmissionSerializer(SubmissionQueueSerializer):
    class Meta:
        model = Submission
        fields = ('id',
                  'problem',
                  'submitted_at',
                  'language',
                  'status',
                  'time_taken',
                  'memory_taken')


class UserRankingSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id',
                  'rank',
                  'email',
                  'location',
                  'solved_problems',
                  'sent_submissions')