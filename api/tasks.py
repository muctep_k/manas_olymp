import base64
import json

import dramatiq
from django.core.mail import send_mail
from django.db import transaction
from django.template.loader import render_to_string

from main.settings import DEFAULT_FROM_EMAIL, DOMAIN_NAME, CALLBACK_URL, JUDGE_URL, DRAMATIQ_DEFAULT_QUEUE


@dramatiq.actor
def send_email_verification(verification_token: str,
                            user_email: str) -> None:
    """
    Send email verification
    """
    email_confirm_url = f'{DOMAIN_NAME}/confirm_register/' \
                        f'{verification_token}'
    context = {
        'email': user_email,
        'email_confirm_url': email_confirm_url,
    }

    contents = {
        'subject': f'Manas Olymp needs to verify your email',
        'html': render_to_string('user_email_verification.html', context),
        'plain_text_content': 'To finish setting up your main account, '
                              'we need to verify this email address belongs to you.'
    }
    send_mail(from_email=DEFAULT_FROM_EMAIL,
              recipient_list=[user_email, ],
              subject=contents['subject'],
              html_message=contents['html'],
              message=contents['plain_text_content'])


@dramatiq.actor
def send_submission_to_judge(submission_id):
    print("HELLO")

    from api.models.app import Submission
    from api.models import TestSubmission
    submission = Submission.objects.prefetch_related('testsubmission_set').select_related('problem').get(id=submission_id)
    data = {'submissions':[]}
    test_submissions = []
    test_cases = submission.testsubmission_set.all()
    for test_case in test_cases:
        time_limit = submission.problem.time_limit if submission.problem.time_limit else 1
        memory_limit = submission.problem.memory_limit if submission.problem.memory_limit else 64000
        data['submissions'].append({'source_code': submission.code, 'language_id': submission.language.id, 'stdin': test_case.test_case.input,
                                    'expected_output': test_case.test_case.output, 'callback_url': CALLBACK_URL,
                                    'cpu_time_limit': time_limit,
                                    'extra_cpu_time_limit': 0.0, 'memory_limit': memory_limit,})
        test_submissions.append(test_case)
    import requests
    with transaction.atomic():
        response = requests.post(f'{JUDGE_URL}/submissions/batch/', json=data)
        for index, data in enumerate(response.json()):
            test_submissions[index].judge_token = data['token']
        TestSubmission.objects.bulk_update(test_submissions, fields=['judge_token'])


@dramatiq.actor(queue_name=DRAMATIQ_DEFAULT_QUEUE)
def update_ranking():

    from api.models import User
    users = User.objects.filter(is_staff=False).order_by('solved_problems', '-sent_submissions')
    for rank, user in enumerate(users):
        user.rank = rank+1
    User.objects.bulk_update(users, fields=['rank'])




