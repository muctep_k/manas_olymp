from datetime import datetime
from os import environ
from random import randint

import jwt
from django.core.mail import send_mail
from django.template.loader import render_to_string
from rest_framework.exceptions import PermissionDenied
from rest_framework.fields import EmailField

# 

# TODO: has to be defined in main.settings.py

EMAIL_CONFIRM_URL = f'https://example.com/api/auth/email-confirmation'
DEFAULT_FROM_EMAIL = environ.get('DEFAULT_FROM_EMAIL', '')
SECRET_KEY = environ.get('SECRET_KEY', '')


class VerificationToken(object):
    signing_key = SECRET_KEY

    def generate_token(self,
                       user_id: int,
                       additional_payload: dict = None,
                       exp: datetime = None) -> str:
        payload = {
            'user_id': user_id
        }
        if exp:
            payload.update({'exp': exp})

        if additional_payload:
            payload.update(additional_payload)
        verification_token = jwt.encode(payload=payload,
                                        key=self.signing_key,
                                        algorithm='HS256')

        return verification_token.decode("utf-8")

    @staticmethod
    def get_token_payload(token: str) -> dict:
        try:
            return jwt.decode(token, SECRET_KEY, algorithms=['HS256'])
        except jwt.ExpiredSignatureError:
            raise PermissionDenied('Sorry, this link has expired.')
        except jwt.DecodeError:
            raise PermissionDenied('Sorry, this link is invalid')


class EmailLowerField(EmailField):
    def to_internal_value(self, data):
        # We're lenient with allowing basic numerics to be coerced into strings,
        # but other types should fail. Eg. unclear if booleans should represent as `true` or `True`,
        # and composites such as lists are likely user error.
        if isinstance(data, bool) or not isinstance(data, (str, int, float,)):
            self.fail('invalid')
        value = str(data)
        value = value.strip() if self.trim_whitespace else value
        return value.lower()


