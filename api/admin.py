from django.contrib import admin
from django_better_admin_arrayfield.admin.mixins import DynamicArrayMixin

from api.models import User, Tags, Problem, TestCase
from api.models.app import ProgrammingLanguage, Submission, PROCESSING_SUBMISSION_STATUS


class TabularTestCase(admin.TabularInline):
    model = TestCase


class ProblemAdmin(admin.ModelAdmin, DynamicArrayMixin):
    model = Problem
    inlines = [TabularTestCase]


class SubmissionAdmin(admin.ModelAdmin):
    model = Submission
    list_display = ('user', 'status', 'time_taken', 'memory_taken', 'language')
    list_filter = ('status', 'language')

    def get_queryset(self, request):
        return Submission.objects.exclude(status=PROCESSING_SUBMISSION_STATUS)


admin.site.register(User)
admin.site.register(Tags)
admin.site.register(Problem, ProblemAdmin)
admin.site.register(ProgrammingLanguage)
admin.site.register(Submission, SubmissionAdmin)