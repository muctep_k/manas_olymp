from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models
from django.utils.translation import gettext as _


class User(AbstractUser):
    password = models.CharField(_('password'), max_length=128, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True,
                                      null=True,
                                      blank=True)
    updated_at = models.DateTimeField(auto_now=True,
                                      null=True)

    objects = UserManager()

    email = models.EmailField(_('email address'),
                              unique=True)
    is_active = models.BooleanField(
        _('active'),
        default=False,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    username = models.CharField(
        _('username'),
        max_length=150,
        unique=False,
        help_text=_(
            'Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        error_messages={
            'unique': _("A user with that username already exists."),
        },
        null=True,
        blank=True
    )
    photo = models.ImageField(_("Photo"), upload_to='user_photos', null=True, blank=True)
    location = models.CharField(_("Location"), max_length=256, null=True, blank=True)
    last_activity = models.DateTimeField(_("Last Activity"), max_length=256, null=True, blank=True)
    rank = models.IntegerField(_("Rank"), default=0)
    solved_problems = models.IntegerField(_("Solved Problems"), default=0)
    sent_submissions = models.IntegerField(_("Submissions"), default=0)
    accepted_submissions = models.IntegerField(_("Accepted submissions"), default=0)
    full_name = models.CharField(_("Full name"), max_length=128, null=True, blank=True)

    USERNAME_FIELD = 'email'

    REQUIRED_FIELDS = []

    def __str__(self):
        return self.email
