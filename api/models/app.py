from ckeditor_uploader.fields import RichTextUploadingField
from django.views.generic import DeleteView
from django_better_admin_arrayfield.models.fields import ArrayField
from django.db import models
from django.utils.translation import gettext as _

from api.models import User

PROBLEM_LEVELS = [
    ('very_easy', 'Very Easy'),
    ('easy', 'Easy'),
    ('medium', 'Medium'),
    ('difficult', 'Difficult'),
    ('very_difficult', 'Very Difficult')
]


PROCESSING_SUBMISSION_STATUS = "Processing"
ACCEPTED_SUBMISSION_STATUS = "Accepted"


class Tags(models.Model):
    title = models.CharField(_("Title"), max_length=64)

    def __str__(self):
        return self.title


class Problem(models.Model):
    title = models.CharField(_("Title"), max_length=64)
    description = RichTextUploadingField(_("Description"),)
    time_limit = models.FloatField(_("Time Limit"))
    memory_limit = models.FloatField(_("Memory limit"))
    input_example = ArrayField(verbose_name=_("Input example"), base_field=models.TextField(_("Single input example")))
    output_example = ArrayField(verbose_name=_("Output example"), base_field=models.TextField(_("Single output example")))
    complexity = models.CharField(_("Complexity"), choices=PROBLEM_LEVELS, max_length=64)
    tags = models.ManyToManyField(to=Tags, related_name='problems')
    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    all_submissions = models.IntegerField(_("All submissions"), default=0)
    successful_submissions = models.IntegerField(_("Successful submissions"), default=0)
    is_public = models.BooleanField(_("Public or not"), default=True)

    def __str__(self):
        return self.title


class ProgrammingLanguage(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(_("Language"), max_length=256)

    def __str__(self):
        return self.name


class TestCase(models.Model):
    problem = models.ForeignKey(to=Problem, related_name='test_cases', on_delete=models.CASCADE)
    input = models.TextField(_("Input"))
    output = models.TextField(_("Output"))


class Submission(models.Model):
    user = models.ForeignKey(to=User, related_name='submissions', on_delete=models.CASCADE)

    problem = models.ForeignKey(to=Problem, related_name='submissions', on_delete=models.CASCADE)
    submitted_at = models.DateTimeField(_("Submitted at"), auto_now_add=True)
    code = models.TextField(_("Submission code"))
    status = models.CharField(_("Status"), max_length=128, default=PROCESSING_SUBMISSION_STATUS)
    language = models.ForeignKey(to=ProgrammingLanguage, related_name='submissions', on_delete=models.CASCADE)
    time_taken = models.FloatField(_("Time Taken by testcase"), null=True, blank=True)
    memory_taken = models.FloatField(_("Memory Taken by testcase"), null=True, blank=True)

    @property
    def final_time_taken(self):
        result = [submission.time_taken for submission in self.testsubmission_set.all() if submission.time_taken]
        if result:
            return max(result)
        return None

    @property
    def final_memory_taken(self):
        result = [submission.memory_taken for submission in self.testsubmission_set.all() if submission.memory_taken]
        if result:
            return max(result)
        return None

    @property
    def final_status(self):
        non_processing = [testsubmission for testsubmission in self.testsubmission_set.all()
                          if testsubmission != PROCESSING_SUBMISSION_STATUS]
        if non_processing:
            status = ACCEPTED_SUBMISSION_STATUS
            error_status = [testsubmission for testsubmission in non_processing
                            if testsubmission != ACCEPTED_SUBMISSION_STATUS]
            if error_status:
                status = error_status[0].status
            return status
        return PROCESSING_SUBMISSION_STATUS

    @property
    def percentage(self):
        accepted_test_cases = len([testsubmission for testsubmission in self.testsubmission_set.all()
                          if testsubmission == ACCEPTED_SUBMISSION_STATUS])
        total_len = len(self.testsubmission_set.all())
        if accepted_test_cases:
            return accepted_test_cases/total_len * 100.0
        return 0


class TestSubmission(models.Model):
    test_case = models.ForeignKey(to=TestCase, on_delete=models.CASCADE)
    submission = models.ForeignKey(to=Submission, on_delete=models.CASCADE)
    judge_token = models.CharField(_("Judge token"), max_length=256, null=True, blank=True)
    output = models.TextField(_("Output"), null=True, blank=True)
    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    status = models.CharField(_("Status"), max_length=128, default=PROCESSING_SUBMISSION_STATUS)
    time_taken = models.FloatField(_("Time Taken by testcase"), null=True, blank=True)
    memory_taken = models.FloatField(_("Memory Taken by testcase"), null=True, blank=True)


class Assignment(models.Model):
    name = models.CharField(_("Name"), max_length=256)
    description = RichTextUploadingField(_("Description"),)
    start_date = models.DateTimeField(_("Start date"))
    end_date = models.DateTimeField(_("End date"))
    access_code = models.CharField(_("Access Code"), unique=True, max_length=256)
    created_by = models.ForeignKey(to='User', on_delete=models.SET_NULL, related_name='tournaments', null=True)
    problems = models.ManyToManyField(to='Problem', related_name='tournaments')


class AssignmentRanking(models.Model):
    assignment = models.ForeignKey(to=Assignment, related_name='rankings', on_delete=models.CASCADE)
    user = models.ForeignKey(to=User, related_name='rankings', on_delete=models.CASCADE)
    rank = models.IntegerField()
    solved_count = models.IntegerField()
