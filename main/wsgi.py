import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'main.settings')

application = get_wsgi_application()


def start_scheduler():
    from main.scheduler import scheduler
    if not scheduler.running:
        scheduler.start()


start_scheduler()
