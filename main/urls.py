from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include, re_path
from rest_framework_swagger.views import get_swagger_view

from django.views.generic import TemplateView
from django.views.decorators.cache import never_cache

# Serve Single Page Application
index = never_cache(TemplateView.as_view(template_name='index.html'))

schema_view = get_swagger_view(title='Manas Olymp Back-End Back End')

urlpatterns = [
    path('', index, name='index'),
    path('admin/', admin.site.urls),
    path('api/', include(('api.urls', 'api'))),
url(r'^silk/', include('silk.urls', namespace='silk')),
    re_path(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^(?:.*)/?$', index),
]
