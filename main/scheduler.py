import logging

import dramatiq
from apscheduler.jobstores.redis import RedisJobStore
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.cron import CronTrigger
from django.contrib.auth import get_user_model
from django.core import management
from dramatiq import Message
from pytz import utc

__all__ = ['scheduler']

from main import settings
from main.settings import REDIS_HOST, SCHEDULER_DEFAULT_QUEUE, REDIS_PORT, REDIS_PASSWORD

logging.basicConfig()
logging.getLogger('apscheduler').setLevel(logging.DEBUG)

jobs_key = f'apscheduler:{SCHEDULER_DEFAULT_QUEUE}.jobs'
run_times_key = f'apscheduler:{SCHEDULER_DEFAULT_QUEUE}.run_times'

jobstores = {
    'default': RedisJobStore(host=REDIS_HOST,
                             port=REDIS_PORT,
                             password=REDIS_PASSWORD,
                             jobs_key=jobs_key,
                             run_times_key=run_times_key,
                             health_check_interval=30),
}
job_defaults = {
    'coalesce': True,
    'max_instances': 1,
    'misfire_grace_time': 15 * 60,  # 15 minutes
}
scheduler = BackgroundScheduler(jobstores=jobstores,
                                job_defaults=job_defaults,
                                timezone=utc,
                                deamon=True)

every_hour = CronTrigger.from_crontab('0 * * * *')  # every hour
every_minute = CronTrigger.from_crontab('* * * * *')  # every minute

UserModel = get_user_model()


def enqueue_dramatiq_actor(actor_name: str,
                           actor_args: tuple = None,
                           actor_kwargs: dict = None,
                           actor_options: dict = None):
    dramatiq.get_broker().enqueue(
        Message(
            queue_name=settings.DRAMATIQ_DEFAULT_QUEUE,
            actor_name=actor_name,
            args=actor_args or (),
            kwargs=actor_kwargs or {},
            options=actor_options or {},
        )
    )


scheduler.add_job(
    id='update_ranking',
    func=enqueue_dramatiq_actor,
    trigger=every_minute,
    replace_existing=True,
    kwargs={
        'actor_name': 'update_ranking'
    })