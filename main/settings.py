import sys
from datetime import timedelta, time
from os import environ, path, getenv
import django_heroku
import redis

BASE_DIR = path.dirname(path.dirname(path.abspath(__file__)))

SECRET_KEY = environ.get('SECRET_KEY', "vyvkyoaRznwwXRUcCtlaokHfWOEMbNZsdpsalSxqEPeuBrnGwplLOqpFPkZa")

DEBUG = environ.get('DEBUG', True)
THUMBNAIL_DEBUG = DEBUG

ALLOWED_HOSTS = [
    environ.get('DOMAIN', '*')
]


STORAGE_PATHS = {}

STORAGE_PATHS['BASE'] = ''
STORAGE_PATHS['IMAGES'] = path.join(STORAGE_PATHS['BASE'], 'images')
STORAGE_PATHS['FILES'] = path.join(STORAGE_PATHS['BASE'], 'files')

# STORAGE_PATHS['IMAGES_SOMETHING'] = path.join(STORAGE_PATHS['IMAGES'], 'something')

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'whitenoise.runserver_nostatic',
    'django.contrib.staticfiles',
    #
    'silk',
    'ckeditor',
    'ckeditor_uploader',
    'django_better_admin_arrayfield',
    'rest_framework',
    'rest_framework.authtoken',
    'rest_framework_simplejwt',
    'rest_framework_swagger',
    'corsheaders',
    'drf_yasg',
    'django_dramatiq',
    'storages',
    'api'
]

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'silk.middleware.SilkyMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'main.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            path.abspath(path.join(BASE_DIR, 'front/build')),
            path.abspath(path.join(BASE_DIR, 'templates'))
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'main.wsgi.application'

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        'OPTIONS': {
            'min_length': 6,
        },
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        # 'file': {
        #     'level': 'DEBUG',
        #     'class': 'logging.FileHandler',
        #     'filename': path.join(BASE_DIR, 'log/debug.log'),
        # },
        'console-stdout': {
            'class': 'logging.StreamHandler',
            'stream': sys.stdout,
        },
    },
    'loggers': {
        # 'django-file': {
        #     'handlers': ['file'],
        #     'level': 'DEBUG',
        #     'propagate': True,
        # },
        'django.request': {
            'handlers': ['console-stdout'],
        },
    },
}

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Asia/Bishkek'

USE_I18N = True

USE_L10N = True

USE_TZ = True

SITE_ID = 1

REST_FRAMEWORK = {
    'EXCEPTION_HANDLER': 'api.exceptions.custom_exception_handler',
    'AUTH_HEADER_TYPES': 'Token',
    'NON_FIELD_ERRORS_KEY': 'object_error',
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAuthenticated',
    ],
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework_simplejwt.authentication.JWTAuthentication',
    ),
}

CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_CREDENTIALS = True

GRAPH_MODELS = {
    'all_applications': True,
    'group_models': True,
}
SIMPLE_JWT = {
    'ACCESS_TOKEN_LIFETIME': timedelta(minutes=30),
    'REFRESH_TOKEN_LIFETIME': timedelta(days=30),
    'USER_ID_CLAIM': 'user_id'
}

AUTH_USER_MODEL = 'api.User'

# UPLOADS
FILE_UPLOAD_MAX_MEMORY_SIZE = 33554432  # 100mb
DATA_UPLOAD_MAX_MEMORY_SIZE = 524288000  # 500mb



# DRAMATIQ CONF
DRAMATIQ_REDIS_URL = environ.get('REDIS_URL', f'redis://127.0.0.1:6379/0')

pool = redis.ConnectionPool.from_url(DRAMATIQ_REDIS_URL)
REDIS_HOST = pool.connection_kwargs.get('host')
REDIS_PORT = pool.connection_kwargs.get('port')
REDIS_PASSWORD = pool.connection_kwargs.get('password')
DRAMATIQ_BROKER = {
    "BROKER": "dramatiq.brokers.redis.RedisBroker",
    "OPTIONS": {
        "connection_pool": pool,
    },
    "MIDDLEWARE": [
        "dramatiq.middleware.AgeLimit",
        "dramatiq.middleware.TimeLimit",
        "dramatiq.middleware.Retries",
        "django_dramatiq.middleware.AdminMiddleware",
        "django_dramatiq.middleware.DbConnectionsMiddleware",
    ]
}
DRAMATIQ_TASKS_DATABASE = "default"
DRAMATIQ_DEFAULT_QUEUE = 'manas-olymp-queue'
SCHEDULER_DEFAULT_QUEUE = 'manas-olymp-queue'



# # AWS
AWS_S3_ACCESS_KEY_ID = environ.get('S3_KEY')
AWS_S3_SECRET_ACCESS_KEY = environ.get('S3_SECRET')
AWS_STORAGE_BUCKET_NAME = environ.get('S3_BUCKET')
# AWS_S3_REGION = environ.get('S3_REGION')
# AWS_LOCATION = environ.get('AWS_LOCATION', 'static')
AWS_DEFAULT_ACL = 'public-read'
# # Cause Python does not parse environment variables to Python objects, it just gets them as strings
# _AWS_QUERYSTRING_AUTH = environ.get('AWS_QUERYSTRING_AUTH')
AWS_QUERYSTRING_AUTH = False
# AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME
#
# # STATIC/MEDIA FILES MANAGMENT
# STATICFILES_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
# STATIC_URL = 'https://%s/%s/' % (AWS_S3_CUSTOM_DOMAIN, AWS_LOCATION)
# MEDIAFILES_LOCATION = 'media'
DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
#
# ADMIN_MEDIA_PREFIX = STATIC_URL + 'admin/'

# STATICFILES_FINDERS = [
#     'django.contrib.staticfiles.finders.FileSystemFinder',
#     'django.contrib.staticfiles.finders.AppDirectoriesFinder',
# ]

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
)

# CKEditor UI and plugins configuration
CKEDITOR_CONFIGS = {
    'default': {
        # Toolbar configuration
        # name - Toolbar name
        # items - The buttons enabled in the toolbar
        'toolbar_DefaultToolbarConfig': [
            {
                'name': 'basicstyles',
                'items': ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript',
                          'Superscript', ],
            },
            {
                'name': 'clipboard',
                'items': ['Undo', 'Redo', ],
            },
            {
                'name': 'paragraph',
                'items': ['NumberedList', 'BulletedList', 'Outdent', 'Indent',
                          'HorizontalRule', 'JustifyLeft', 'JustifyCenter',
                          'JustifyRight', 'JustifyBlock', ],
            },
            {
                'name': 'format',
                'items': ['Format', ],
            },
            {
                'name': 'extra',
                'items': ['Link', 'Unlink', 'Blockquote', 'Image', 'Table',
                          'CodeSnippet', 'Mathjax', 'Embed', ],
            },
            {
                'name': 'source',
                'items': ['Maximize', 'Source', ],
            },
        ],

        # This hides the default title provided by CKEditor
        'title': False,

        # Use this toolbar
        'toolbar': 'DefaultToolbarConfig',

        # Which tags to allow in format tab
        'format_tags': 'p;h1;h2',

        # Remove these dialog tabs (semicolon separated dialog:tab)
        'removeDialogTabs': ';'.join([
            'image:advanced',
            'image:Link',
            'link:upload',
            'table:advanced',
            'tableProperties:advanced',
        ]),
        'linkShowTargetTab': False,
        'linkShowAdvancedTab': False,

        # CKEditor height and width settings
        'height': '250px',
        'width': 'auto',
        'forcePasteAsPlainText ': True,

        # Class used inside span to render mathematical formulae using latex
        'mathJaxClass': 'mathjax-latex',

        # Mathjax library link to be used to render mathematical formulae
        'mathJaxLib': 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.9/MathJax.js?config=TeX-AMS_SVG',

        # Tab = 4 spaces inside the editor
        'tabSpaces': 4,

        # Extra plugins to be used in the editor
        'extraPlugins': ','.join([
            # 'devtools',  # Shows a tooltip in dialog boxes for developers
            'mathjax',  # Used to render mathematical formulae
            'codesnippet',  # Used to add code snippets
            'image2',  # Loads new and better image dialog
            'embed',  # Used for embedding media (YouTube/Slideshare etc)
            'tableresize',  # Used to allow resizing of columns in tables
        ]),
    }
}
# Configure app for Heroku deployment
django_heroku.settings(locals())
DATABASES = {
    'default': {
        #
        'ENGINE': 'django.db.backends.postgresql',
        #
        'HOST': '206.81.23.176',
        'PORT': 5432,
        'NAME': 'manas_olymp',
        'USER': 'kutman',
        'PASSWORD': 'manas_olymp_pass',
    }
}
DATABASES['default']['CONN_MAX_AGE'] = 0

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/
STATIC_URL = '/static/'
STATIC_ROOT = path.join(BASE_DIR, 'staticfiles')
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'


REACT_APP_DIR = path.join(BASE_DIR, 'front')

STATICFILES_DIRS = [
    path.join(REACT_APP_DIR, 'build', 'static')
]

# If you want to serve user uploaded files add these settings
MEDIA_URL = '/media/'
MEDIA_ROOT = 'media'
CKEDITOR_UPLOAD_PATH = "uploads/"
# EMAIL CONF
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_USE_TLS = True
EMAIL_HOST = environ.get('EMAIL_HOST', '')
EMAIL_PORT = environ.get('EMAIL_PORT', '')
EMAIL_HOST_USER = environ.get('EMAIL_HOST_USER', '')
EMAIL_HOST_PASSWORD = environ.get('EMAIL_HOST_PASSWORD', '')
DEFAULT_FROM_EMAIL = environ.get("DEFAULT_FROM_EMAIL", "")
DOMAIN_NAME = environ.get("DOMAIN_NAME", "")

#JUDGE CONF
JUDGE_URL = environ.get('JUDGE_URL', 'http://206.81.23.176:2358/')
CALLBACK_URL = environ.get('CALLBACK_URL', 'http://d6583c8d1849.ngrok.io/api/callback/')

try:
    from .local_settings import *
except ImportError:
    try:
        from local_settings import *
    except ImportError:
        print('Local settings couldn\'t be imported.')
