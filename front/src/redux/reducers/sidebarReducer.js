import {SET} from "../types/sidebarTypes"

const initialState = {
  sidebarShow: false
}

export default function sidebarReducer(state = initialState, action){
  switch (action.type) {
    case SET:
      return {...state, sidebarShow: action.payload }
    default:
      return state
  }
}
