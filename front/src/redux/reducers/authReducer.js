import {
  USER_LOADED,
  USER_LOADING,
  USER_FAIL,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT_SUCCESS,
  LOGIN_LOADING,
  CLEAR_ERROR,
  REGISTER_LOADING,
  REGISTER_FAIL,
  REGISTER_SUCCESS, REGISTER_SMS_SENT,
} from '../types/authTypes'

const initialState = {
  access: JSON.parse(localStorage.getItem('access_token_manas')),
  refresh: JSON.parse(localStorage.getItem('refresh_token_manas')),
  isAuthenticated: false,
  user: null,
  error: {},
  isUserLoading: true,
  isLoginLoading: false,
  isRegisterLoading: false,
  isSmsConfirmLoading: false,
  hasSmsConfirmed: false
}

export default function authReducer(state = initialState, action) {
  switch (action.type) {
    case USER_LOADING:
      return {
        ...state,
        error: {},
        isUserLoading: true
      };
    case LOGIN_LOADING:
      return {
        ...state,
        isLoginLoading: true,
        error: {}
      }
    case REGISTER_LOADING:
      return {
        ...state,
        isRegisterLoading: true,
        error: {}
      }
    case REGISTER_SMS_SENT:
      return {
        ...state,
        isRegisterLoading: false,
        error: {}
      }
    case REGISTER_SUCCESS:
      return {
        ...state,
        isSmsConfirmLoading: false,
        hasSmsConfirmed: true,
        error: null,
        isRegisterLoading: false
      }
    case USER_LOADED:
      localStorage.setItem('access_token_manas', JSON.stringify(action.payload.access))
      return {
        ...state,
        isAuthenticated: true,
        user: action.payload.user,
        error: {},
        isUserLoading: false,
        access: action.payload.access
      };
    case LOGIN_SUCCESS:
      localStorage.setItem('access_token_manas', JSON.stringify(action.payload.access))
      localStorage.setItem('refresh_token_manas', JSON.stringify(action.payload.refresh))
      return {
        ...state,
        ...action.payload,
        access: action.payload.access,
        refresh: action.payload.refresh,
        user: action.payload.user,
        isAuthenticated: true,
        isLoginLoading: false,
        isRegisterLoading: false,
        error: {}
      };
    case USER_FAIL:
    case LOGIN_FAIL:
    case LOGOUT_SUCCESS:
    case REGISTER_FAIL:
      localStorage.removeItem('access_token_manas');
      localStorage.removeItem('refresh_token_manas');
      return {
        ...state,
        access: null,
        refresh: null,
        user: null,
        isAuthenticated: false,
        isLoginLoading: false,
        isRegisterLoading: false,
        isUserLoading: false,
        error: action.payload
      }
    case CLEAR_ERROR:
      return {
        ...state,
        error: {}
      };
    default:
      return {
        ...state
      };
  }
}
