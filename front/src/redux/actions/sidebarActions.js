import {SET} from "../types/sidebarTypes"

export const set = sidebarShow => {
  return {
    type: SET,
    payload: sidebarShow
  }
}
