import {
  USER_LOADED,
  USER_LOADING,
  LOGOUT_SUCCESS,
  LOGIN_FAIL,
  LOGIN_SUCCESS,
  LOGIN_LOADING,
  CLEAR_ERROR,
  REGISTER_LOADING,
  REGISTER_FAIL,
  REGISTER_SUCCESS,
  USER_FAIL, REGISTER_SMS_SENT,
} from "../types/authTypes"
import {_baseApi, doRequestAndParse} from "../../services/manasService";

export const loadUser = () => async (dispatch, getState) => {
  const refresh = getState().auth.refresh
  if (refresh){
    dispatch(userLoading())
    const result = await doRequestAndParse(`${_baseApi}/refresh_token/`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ refresh })
    })

    const { success, data } = result

    if (success){
      dispatch(userLoaded({ access: data.access, user: data.user }))
    } else {
      dispatch(userFail(data && data.detail && data.object_error && data.object_error[0]))
    }
  } else {
    dispatch(userFail(''))
  }
}

export const login = (email, password) => async dispatch => {
  dispatch(loginLoading())

  const result = await doRequestAndParse(`${_baseApi}/login/`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({email, password})
  })

  const { success, data } = result

  console.log('data: ', data)

  if (success){
    const { refresh, access, email } = data
    dispatch(loginSuccess({ refresh, access, user: {email}}))
  } else {
    dispatch(loginFail(data.detail))
  }
}

export const register = (first_name, last_name, email, password, confirm_password) => async dispatch => {
  dispatch(registerLoading())

  const result = await doRequestAndParse(`${_baseApi}/register/`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ first_name, last_name, email, password, confirm_password })
  })

  const { success, data } = result

  success ? dispatch(registerSmsSent()) : dispatch(registerFail(data.detail))
}

export const confirmRegister = token => async dispatch => {
  dispatch(registerSmsLoading())

  const result = await doRequestAndParse(`${_baseApi}/register/?token=${token}`)

  const { success, data } = result

  if (success){
    dispatch(registerSuccess())
  } else {
    dispatch(registerFail(data?.detail?.object_error || data?.detail?.object_error[0]))
  }
}

const registerLoading = () => ({ type: REGISTER_LOADING })
const registerFail = payload => ({ type: REGISTER_FAIL, payload })

const registerSmsSent = () => ({ type: REGISTER_SMS_SENT })
const registerSmsLoading = () => ({ type: REGISTER_LOADING })
const registerSuccess = () => ({ type: REGISTER_SUCCESS })

const loginLoading = () => ({ type: LOGIN_LOADING })
const loginFail = payload => ({ type: LOGIN_FAIL, payload })
const loginSuccess = payload => ({ type: LOGIN_SUCCESS, payload })

const userLoading = () => ({ type: USER_LOADING })
const userFail = () => ({ type: USER_FAIL })
const userLoaded = payload => ({ type: USER_LOADED, payload })

export const clearError = () => ({ type: CLEAR_ERROR })
export const logout = () => ({ type: LOGOUT_SUCCESS })
