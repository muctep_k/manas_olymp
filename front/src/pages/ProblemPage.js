import React from 'react'
import {TheFooter, TheHeader, TheSidebar} from "../containers"
import ProblemContent from "../components/problem/ProblemContent"

function ProblemPage(props) {

  return (
    <div className="c-app c-default-layout">
      <TheSidebar/>
      <div className="c-wrapper">
        <TheHeader/>
        <div className="c-body">
          <ProblemContent/>
        </div>
        <TheFooter/>
      </div>
    </div>
  )
}

export default ProblemPage
