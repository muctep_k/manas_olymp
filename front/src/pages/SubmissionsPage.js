import React from 'react'
import {TheFooter, TheHeader, TheSidebar} from "../containers"
import SubmissionsContent from "../components/submissions/SubmissionsContent"

function SubmissionsPage(props) {

  return (
    <div className="c-app c-default-layout">
      <TheSidebar/>
      <div className="c-wrapper">
        <TheHeader/>
        <div className="c-body">
          <SubmissionsContent/>
        </div>
        <TheFooter/>
      </div>
    </div>
  )
}

export default SubmissionsPage
