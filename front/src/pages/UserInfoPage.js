import React, {useCallback, useContext, useEffect, useState} from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CLabel,
  CInput,
  CForm,
  CFormText,
  CFormGroup,
  CModalHeader,
  CModalFooter,
  CContainer,
  CRow,
  CCol,
  CButton
} from "@coreui/react"
import CIcon from "@coreui/icons-react"
import SidebarWrapper from "../containers/SidebarWrapper"
import NoPhoto from '../assets/images/no-photo.png'
import CameraIcon from '../assets/icons/camera-96.png'
import ManasServiceContext from "../services/manasServiceContext";
import {convertBase64} from "../helpers/functions";
import {toast} from "react-toastify";
import MiniSpinner from "../components/spinners/MiniSpinner"
import {useTranslation} from "react-i18next"
import {useParams} from "react-router-dom"
import FullContentSpinner from "../components/spinners/FullContentSpinner";

const initialInputs = { full_name: '', address: '', password: '', confirm_password: ''}

const initialErrors = { full_name: '', address: '', password: '', confirm_password: '' }

function UserInfoPage(props) {

  const manasService = useContext(ManasServiceContext)
  const {t} = useTranslation('common')

  const {userId} = useParams()

  const [profile, setProfile] = useState(null)
  const [isProfileLoading, setIsProfileLoading] = useState(false)

  const [inputs, setInputs] = useState(initialInputs)

  useEffect(() => {
    if (profile){
      setInputs(state => ({
        ...state,
        full_name: profile.full_name || '',
        address: profile.location || '',
      }))
    }
  }, [profile])

  const fetchProfile = useCallback(async () => {
    const result = await manasService.getProfile(userId)

    if (result.success){
      setProfile(result.data)
    }
  }, [userId])

  useEffect(() => {
    setIsProfileLoading(true)
    fetchProfile().then(() => setIsProfileLoading(false))
  }, [])

  return (
    <>
      <SidebarWrapper>
        <form className="pr-body">
          <CContainer className="pr-container mt-3">
            {
              isProfileLoading ? <FullContentSpinner/> :
                <CRow>
                  <CCol md={4}>
                    <CCard>
                      <CCardBody>
                        <div className="pr-left-container">
                          <div className="custom-photo-upload-wrap">
                            <div className="custom-photo-upload">
                              <label htmlFor="photo-input"/>
                              <img className="image-preview" src={profile?.photo || NoPhoto} alt=""/>
                            </div>
                          </div>
                          <CFormGroup className="mt-4">
                            <CLabel>Email</CLabel>
                            <CInput
                              value={profile?.email}
                              disabled
                            />
                          </CFormGroup>
                          <CFormGroup className="mt-3">
                            <CLabel>{t('profile.registerDate')}</CLabel>
                            <CInput
                              value={profile?.date_joined}
                              disabled
                            />
                          </CFormGroup>
                        </div>
                      </CCardBody>
                    </CCard>
                  </CCol>
                  <CCol md={8}>
                    <CCard>
                      <CCardBody>
                        <CRow>
                          <CCol>
                            <CFormGroup>
                              <CLabel>{t('profile.rating')}</CLabel>
                              <CInput
                                value={profile?.rank}
                                disabled
                              />
                            </CFormGroup>
                            <CFormGroup>
                              <CLabel>{t('profile.solvedProblems')}</CLabel>
                              <CInput
                                value={profile?.solved_problems}
                                disabled
                              />
                            </CFormGroup>
                          </CCol>
                          <CCol>
                            <CFormGroup>
                              <CLabel>{t('profile.allSubmissions')}</CLabel>
                              <CInput
                                value={profile?.sent_submissions}
                                disabled
                              />
                            </CFormGroup>
                            <CFormGroup>
                              <CLabel>{t('profile.successfulSubmissions')}</CLabel>
                              <CInput
                                value={profile?.accepted_submissions}
                                disabled
                              />
                            </CFormGroup>
                          </CCol>
                        </CRow>
                      </CCardBody>
                    </CCard>
                    <CCard>
                      <CCardBody>
                        <CRow>
                          <CCol>
                            <CFormGroup>
                              <CLabel htmlFor="full_name">{t('profile.fullname')}</CLabel>
                              <CInput
                                id="full_name"
                                name="full_name"
                                disabled
                                value={inputs.full_name}
                              />
                            </CFormGroup>
                          </CCol>
                          <CCol>
                            <CFormGroup>
                              <CLabel htmlFor="address">{t('profile.location')}</CLabel>
                              <CInput
                                id="address"
                                name="address"
                                value={inputs.address}
                                disabled
                              />
                            </CFormGroup>
                          </CCol>
                        </CRow>
                      </CCardBody>
                    </CCard>
                  </CCol>
                </CRow>
            }
          </CContainer>
        </form>
      </SidebarWrapper>
    </>
  )
}

export default UserInfoPage
