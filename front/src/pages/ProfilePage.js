import React, {useCallback, useContext, useEffect, useState} from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CLabel,
  CInput,
  CForm,
  CFormText,
  CFormGroup,
  CModalHeader,
  CModalFooter,
  CContainer,
  CRow,
  CCol,
  CButton
} from "@coreui/react"
import CIcon from "@coreui/icons-react"
import SidebarWrapper from "../containers/SidebarWrapper"
import NoPhoto from '../assets/images/no-photo.png'
import CameraIcon from '../assets/icons/camera-96.png'
import ManasServiceContext from "../services/manasServiceContext";
import {convertBase64} from "../helpers/functions";
import {toast} from "react-toastify";
import MiniSpinner from "../components/spinners/MiniSpinner"
import {useTranslation} from "react-i18next";

const initialInputs = { full_name: '', address: '', password: '', confirm_password: ''}

const initialErrors = { full_name: '', address: '', password: '', confirm_password: '' }

function ProfilePage(props) {

  const manasService = useContext(ManasServiceContext)
  const {t} = useTranslation('common')

  const [profile, setProfile] = useState(null)
  const [isProfileLoading, setIsProfileLoading] = useState(false)

  const [isDataSending, setIsDataSending] = useState(false)

  const [file, setFile] = useState('')
  const [imgPreviewUrl, setImgPreviewUrl] = useState(null)

  const [isEdit, setIsEdit] = useState(false)
  const [inputs, setInputs] = useState(initialInputs)
  const [errors, setErrors] = useState(initialErrors)

  const onInputsChange = useCallback(e => {
    setInputs(state => ({ ...state, [e.target.name]: e.target.value }))
    setErrors(state => ({ ...state, [e.target.name]: ''}))
  }, [])

  useEffect(() => {
    if (profile){
      setInputs(state => ({
        ...state,
        full_name: profile.full_name || '',
        address: profile.location || '',
      }))
    }
  }, [profile])

  const fetchProfile = useCallback(async () => {
    const result = await manasService.getProfile()

    if (result.success){
      setProfile(result.data)
    }
  }, [])

  useEffect(() => {
    setIsProfileLoading(true)
    fetchProfile().then(() => setIsProfileLoading(false))
  }, [])

  const photoUpload = (e) => {
    e.preventDefault();
    if (e.target.files[0]){
      const reader = new FileReader();
      const file = e.target.files[0];
      reader.onloadend = () => {
        setFile(file)
        setImgPreviewUrl(reader.result)
      }
      reader.readAsDataURL(file);
    }
  }

  const onSubmit = useCallback(async e => {
    e.preventDefault()

    if (inputs.password !== inputs.confirm_password){
      setErrors(state => ({ ...state, confirm_password: 'Пароли не совпадают!'}))
      return
    }

    setIsDataSending(true)

    const formData = new FormData()

    formData.append('full_name', inputs.full_name)
    formData.append('location', inputs.address)

    if (inputs.password && inputs.confirm_password){
      formData.append('password', inputs.password)
      formData.append('confirm_password', inputs.confirm_password)
    }

    if (file){
      const fileBase64 = await convertBase64(file)
      // formData.append('photo', fileBase64)
    }

    const result = await manasService.updateProfile(formData)

    if (result.success){
      toast.success('Данные успешно обновлены')
    } else {
      toast.error('Что-то пошло не так')
    }

    console.log('result: ', result)

    setIsDataSending(false)

  }, [inputs, file])

  console.log('inputs: ', inputs)
  console.log('errors: ', errors)

  return (
    <>
      <SidebarWrapper>
        <form onSubmit={onSubmit} className="pr-body">
          <CContainer className="pr-container mt-3">
            <CRow>
              <CCol md={4}>
                <CCard>
                  <CCardBody>
                    <div className="pr-left-container">
                      <div className="custom-photo-upload-wrap">
                        <div className="custom-photo-upload">
                          <label htmlFor="photo-input"/>
                          <label htmlFor="photo-input" className="footer">
                            <img src={CameraIcon} alt=""/>
                          </label>
                          <input id="photo-input" type="file" accept="image/*" onChange={photoUpload}/>
                          <img className="image-preview" src={imgPreviewUrl || profile?.photo || NoPhoto} alt=""/>
                        </div>
                      </div>
                      <CFormGroup className="mt-4">
                        <CLabel>Email</CLabel>
                        <CInput
                          value={profile?.email}
                          disabled
                        />
                      </CFormGroup>
                      <CFormGroup className="mt-3">
                        <CLabel>{t('profile.registerDate')}</CLabel>
                        <CInput
                          value={profile?.date_joined}
                          disabled
                        />
                      </CFormGroup>
                    </div>
                  </CCardBody>
                </CCard>
              </CCol>
              <CCol md={8}>
                <CCard>
                  <CCardBody>
                    <CRow>
                      <CCol>
                        <CFormGroup>
                          <CLabel>{t('profile.rating')}</CLabel>
                          <CInput
                            value={profile?.rank}
                            disabled
                          />
                        </CFormGroup>
                        <CFormGroup>
                          <CLabel>{t('profile.solvedProblems')}</CLabel>
                          <CInput
                            value={profile?.solved_problems}
                            disabled
                          />
                        </CFormGroup>
                      </CCol>
                      <CCol>
                        <CFormGroup>
                          <CLabel>{t('profile.allSubmissions')}</CLabel>
                          <CInput
                            value={profile?.sent_submissions}
                            disabled
                          />
                        </CFormGroup>
                        <CFormGroup>
                          <CLabel>{t('profile.successfulSubmissions')}</CLabel>
                          <CInput
                            value={profile?.accepted_submissions}
                            disabled
                          />
                        </CFormGroup>
                      </CCol>
                    </CRow>
                  </CCardBody>
                </CCard>
                <CCard>
                  <CCardBody>
                    <CRow>
                      <CCol>
                        <span className="float-right pr-edit-btn" onClick={() => setIsEdit(edit => !edit)}>
                          { isEdit ? <CIcon size="lg" name="cil-x"/> : <CIcon size="lg" name="cil-pen"/> }
                        </span>
                      </CCol>
                    </CRow>
                    <CRow>
                      <CCol>
                        <CFormGroup>
                          <CLabel htmlFor="full_name">{t('profile.fullname')}</CLabel>
                          <CInput
                            id="full_name"
                            name="full_name"
                            disabled={!isEdit}
                            onChange={onInputsChange}
                            value={inputs.full_name}
                            placeholder={t('profile.fullnamePlaceholder')}
                          />
                          <CFormText>{t('profile.isNotRequired')}</CFormText>
                        </CFormGroup>
                        <CFormGroup>
                          <CLabel htmlFor="password">{t('profile.newPassword')}</CLabel>
                          <CInput
                            id="password"
                            name="password"
                            value={inputs.password}
                            onChange={onInputsChange}
                            disabled={!isEdit}
                            placeholder={t('profile.newPasswordPlaceholder')}
                          />
                          <CFormText>{t('profile.isNotRequired')}</CFormText>
                        </CFormGroup>
                      </CCol>
                      <CCol>
                        <CFormGroup>
                          <CLabel htmlFor="address">{t('profile.location')}</CLabel>
                          <CInput
                            id="address"
                            name="address"
                            value={inputs.address}
                            onChange={onInputsChange}
                            disabled={!isEdit}
                            placeholder={t('profile.locationPlaceholder')}
                          />
                          <CFormText>{t('profile.isNotRequired')}</CFormText>
                        </CFormGroup>
                        <CFormGroup>
                          <CLabel htmlFor="confirm_password">{t('profile.confirmPassword')}</CLabel>
                          <CInput
                            id="confirm_password"
                            name="confirm_password"
                            value={inputs.confirm_password}
                            onChange={onInputsChange}
                            disabled={!isEdit}
                            className={errors.confirm_password ? 'border-danger' : ''}
                            placeholder={t('profile.confirmPasswordPlaceholder')}
                          />
                          <CFormText>
                            {
                              errors.confirm_password ?
                                <CFormText><h6 className="text-danger">{ errors.confirm_password }</h6></CFormText> :
                                inputs.password.length ? t('profile.required') : t('profile.isNotRequired')
                            }
                          </CFormText>
                        </CFormGroup>
                      </CCol>
                    </CRow>
                  </CCardBody>
                </CCard>
              </CCol>
            </CRow>
            <CButton color="success" type="submit">
              {
                isDataSending ? <MiniSpinner/> : t('profile.saveChanges')
              }
            </CButton>
          </CContainer>
        </form>
      </SidebarWrapper>
    </>
  )
}

export default ProfilePage
