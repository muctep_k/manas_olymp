import React, {useContext, useEffect} from 'react'
import {TheFooter, TheHeader, TheSidebar} from "../containers"
import ManasServiceContext from "../services/manasServiceContext"
import {
  CContainer,
  CCard,
  CCardBody,
  CCardHeader,
  CCardFooter,
  CRow,
  CCol
} from "@coreui/react"
import {news} from "../helpers/news"
import {Link} from "react-router-dom";

const MainPage = () => {

  const manasService = useContext(ManasServiceContext)

  useEffect(() => {


  }, [])

  return (
    <div className="c-app c-default-layout">
      <TheSidebar/>
      <div className="c-wrapper">
        <TheHeader/>
        <div className="c-body">
          <CContainer className="custom-container-xs">
            {
              news.articles.map(n => (
                <CCard className="mt-3">
                  <CCardHeader>
                    { n.published_at }
                  </CCardHeader>
                  <CCardBody>
                    <a href={n.url} className="">
                      <h3 style={{ color: '#3c4b64' }}>{ n.title }</h3>
                    </a>
                    <h5 className="mt-3 mb-4">{ n.description }</h5>
                    <img width="100%" src={n.urlToImage} alt=""/>
                    <div className="mt-3" dangerouslySetInnerHTML={{ __html: n.content }}/>
                  </CCardBody>
                </CCard>
              ))
            }
          </CContainer>
        </div>
        <TheFooter/>
      </div>
    </div>
  )
}

export default MainPage
