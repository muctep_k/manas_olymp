import React from 'react'
import 'react-router-dom'
import {Route, Switch, useRouteMatch} from "react-router-dom"
import RegisterPage from "../RegisterPage"
import LoginPage from "../LoginPage"

function Auth() {

  const { path } = useRouteMatch()

  return (
    <Switch>
      <Route exact path={`${path}/login`} name="login" render={props => <LoginPage {...props}/>} />
      <Route exact path={`${path}/register`} name="register" render={props => <RegisterPage {...props}/>} />
    </Switch>
  )
}

export default Auth
