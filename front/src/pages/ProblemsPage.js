import React from 'react'
import {TheFooter, TheHeader, TheSidebar} from "../containers"
import ProblemsContent from "../components/problems/ProblemsContent"

const ProblemsPage = () => {
  return (
    <div className="c-app c-default-layout">
      <TheSidebar/>
      <div className="c-wrapper">
        <TheHeader/>
        <div className="c-body">
          <ProblemsContent/>
        </div>
        <TheFooter/>
      </div>
    </div>
  )
}

export default ProblemsPage
