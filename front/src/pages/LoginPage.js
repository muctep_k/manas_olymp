import React, { useState, useCallback, useEffect } from 'react'
import {Link, useHistory} from 'react-router-dom'
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow,
  CSpinner
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import {useDispatch, useSelector} from "react-redux"
import {clearError, login} from "../redux/actions/authActions"
import {useTranslation} from "react-i18next";

const nullErrors = {
  email: null,
  password: null,
  confirmPassword: null,
  network: null
}

const LoginPage = () => {

  const dispatch = useDispatch()
  const {t, i18n} = useTranslation('common')

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const history = useHistory()
  const { isAuthenticated, error, isLoginLoading } = useSelector(state => state.auth)

  useEffect(() => {
    dispatch(clearError())
  }, [])

  useEffect(() => {
    if (isAuthenticated){
      history.push('/')
    }
  }, [isAuthenticated])


  const emailChangeHandler = useCallback(event => {
    setEmail(event.target.value)
  }, [])

  const passwordChangeHandler = useCallback(event => {
    setPassword(event.target.value)
  }, [])

  const submitHandler = event => {
    event.preventDefault()
    dispatch(login(email, password))
  }

  const onLangChange = useCallback((e, lang) => {
    e.preventDefault()
    i18n.changeLanguage(lang).then(() => {})
    localStorage.setItem("lang_manas_olymp", lang)
  }, [i18n])

  return (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="8">
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <CForm onSubmit={submitHandler}>
                    <h1>{ t('loginPage.login') }</h1>
                    <p className="text-muted">{ t('loginPage.signInToYourAccount') }</p>
                    <CInputGroup className="mt-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        type="text"
                        placeholder="Email"
                        autoComplete="email"
                        onChange={emailChangeHandler}
                      />
                    </CInputGroup>
                    <div className="auth-error-field">{ error && error.email && error.email[0] }</div>
                    <CInputGroup className="mt-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-lock-locked" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        type="password"
                        placeholder={ t('loginPage.enterPassword') }
                        autoComplete="current-password"
                        onChange={passwordChangeHandler}
                      />
                    </CInputGroup>
                    <div className="auth-error-field">{ error && error.password && error.password[0] }</div>
                    <CRow className="mt-4">
                      <CCol xs="6">
                          <CButton type="submit" color="primary" className="px-4">
                            { isLoginLoading ? <CSpinner color="warning" size="sm" /> : t('loginPage.login') }
                          </CButton>
                      </CCol>
                    </CRow>
                    <div className="auth-error-field">{ error && error.network && 'Проблемы с сетью' }</div>
                  </CForm>
                </CCardBody>
              </CCard>
              <CCard className="text-white bg-primary py-5 d-md-down-none" style={{ width: '44%' }}>
                <CCardBody className="text-center">
                  <div className="mb-5">
                    <a className={`m-1 cursor-pointer ${i18n.language === 'en' ? 'text-success' : ''}`}
                       onClick={e => onLangChange(e, 'en')}>
                      EN
                    </a> |
                    <a className={`m-1 cursor-pointer ${i18n.language === 'ru' ? 'text-success' : ''}`}
                       onClick={e => onLangChange(e, 'ru')}>
                      RU
                    </a> |
                    <a className={`m-1 cursor-pointer ${i18n.language === 'ky' ? 'text-success' : ''}`}
                       onClick={e => onLangChange(e, 'ky')}>
                      KY
                    </a>
                  </div>
                  <div>
                    <h2>{ t('loginPage.signup') }</h2>
                    <p>{ t('loginPage.registerText') }</p>
                    <Link to="/register">
                      <CButton color="primary" className="mt-3" active tabIndex={-1}>{ t('loginPage.registerNow') }</CButton>
                    </Link>
                  </div>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}

export default LoginPage
