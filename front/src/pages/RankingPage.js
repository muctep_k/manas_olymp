import React, {useCallback, useContext, useEffect, useState} from 'react'
import SidebarWrapper from "../containers/SidebarWrapper"
import {
  CCard,
  CCardHeader,
  CPagination,
  CContainer,
  CDataTable,
} from "@coreui/react"
import ManasServiceContext from "../services/manasServiceContext"
import {useHistory} from "react-router-dom"
import FullContentSpinner from "../components/spinners/FullContentSpinner"
import {useQuery} from "../helpers/hooks"
import {useTranslation} from "react-i18next"

function RankingPage(props) {

  const query = useQuery()
  const page = +query.get("page") || 1

  const manasService = useContext(ManasServiceContext)
  const history = useHistory()
  const {t} = useTranslation('common')

  const [rank, setRank] = useState([])
  const [isRankLoading, setIsRankLoading] = useState(false)

  const [currPage, setCurrPage] = useState(1)
  const [pageCount, setPageCount] = useState(1)

  const fetchRank = useCallback(async params => {
    const result = await manasService.getRank(params)

    if (result.success){
      setRank(result.data.results)
      setPageCount(Math.trunc(parseInt(result.data.count) / 50) + 1)
    }

    setIsRankLoading(false)
  }, [page])

  useEffect(() => {
    setIsRankLoading(true)

    const params = `?page=${page}`

    fetchRank(params).then(() => {})
  }, [page])

  const onPageChange = useCallback(page => {
    setCurrPage(page)
  }, [])

  useEffect(() => {
    pushToUrl(currPage)
  }, [currPage])

  const pushToUrl = useCallback((currPage) => {
    let path = `/ranking/?page=${currPage}`
    history.push(path)
  }, [])

  const fields = [
    {
      key: 'rank',
      label: t('ranking.ranking'),
      _style: { width: '25%' },
      filter: false
    },
    {
      key: 'email',
      label: 'Email',
      filter: false
    },
    {
      key: 'location',
      label: t('ranking.location'),
      filter: false
    },
    {
      key: 'sent_submissions',
      label: t('ranking.sentSubmissions'),
      filter: false
    },
    {
      key: 'solved_problems',
      label: t('ranking.solvedProblems'),
      filter: false
    }
  ]

  return (
    <SidebarWrapper>
      <CContainer className="container-sm">
        <CCard className="mt-3">
          <CCardHeader>
            {
              isRankLoading ? <FullContentSpinner/> :
                <>
                  <CDataTable
                    items={rank}
                    fields={fields}
                    columnFilter
                    hover
                    clickableRows
                    onRowClick={item => history.push(`/users/${item.id}`)}
                    scopedSlots = {{
                      'rank':
                        (item)=>(
                          <td>
                            { item.rank }
                          </td>
                        ),
                      'email':
                        (item)=>(
                          <td>
                            { item.email }
                          </td>
                        ),
                      'location':
                        (item)=>(
                          <td>
                            { item.location }
                          </td>
                        ),
                      'sent_submissions':
                        (item)=>(
                          <td>
                            {item.sent_submissions}
                          </td>
                        ),
                      'solved_problems':
                        (item)=>(
                          <td>
                            { item.solved_problems }
                          </td>
                        ),
                    }}
                  />
                  <CPagination
                    activePage={currPage}
                    pages={pageCount}
                    onActivePageChange={onPageChange}
                  />
                </>
            }
          </CCardHeader>
        </CCard>
      </CContainer>
    </SidebarWrapper>
  )
}

export default RankingPage
