import React from 'react'
import {TheFooter, TheHeader, TheSidebar} from "../containers"
import SubmitContent from "../components/submit/SubmitContent"

function SubmitPage() {

  return (
    <div className="c-app c-default-layout">
      <TheSidebar/>
      <div className="c-wrapper">
        <TheHeader/>
        <div className="c-body">
          <SubmitContent/>
        </div>
        <TheFooter/>
      </div>
    </div>
  )
}

export default SubmitPage
