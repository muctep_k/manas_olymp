import React, { useEffect, useState, useContext } from 'react'
import {useParams, useRouteMatch, useLocation, useHistory, Link} from 'react-router-dom'
import ManasServiceContext from "../services/manasServiceContext"
import {
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CRow,
  CSpinner
} from "@coreui/react"
import {useDispatch, useSelector} from "react-redux"
import {confirmRegister} from "../redux/actions/authActions"

const ConfirmRegister = (props) => {

  const manasService = useContext(ManasServiceContext)
  const dispatch = useDispatch()
  const history = useHistory()
  const { token } = useParams()
  const { hasSmsConfirmed, isRegisterLoading, error }  = useSelector(state => state.auth)

  const [isLoading, setIsLoading] = useState(false)

  useEffect(() => {
    const doConfirm = async tkn => dispatch(confirmRegister(tkn))

    if (token){
      doConfirm(token).then(() => {})
    } else {
      history.push('/404')
    }
  }, [])

  return (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          {
            isRegisterLoading ? <CSpinner
              color="primary"
              style={{width:'4rem', height:'4rem'}}
            /> : <CCol md="9" lg="7" xl="6">
              <CCard className="mx-4">
                <CCardBody className="p-4">
                  {
                    error ? <>
                      <h4 className="text-danger">Неправильный токен</h4>
                      <h4 className="text-danger mb-3">Попробуйте еще раз</h4>
                      <div className="d-flex">
                        <Link className="text-decoration-none text-white btn btn-facebook mr-3" to="/register">Регистрация</Link>
                        <Link className="text-decoration-none text-white btn btn-success" to="/">Перейти на главную</Link>
                      </div>
                    </> : <>
                      <h3 className="mb-3">Ваш почтовый адрес успешно подтвержден</h3>
                      <div className="d-flex">
                        <Link className="text-decoration-none text-white btn btn-facebook mr-3" to="/login">Войти</Link>
                        <Link className="text-decoration-none text-white btn btn-success" to="/">Перейти на главную</Link>
                      </div>
                    </>
                  }
                </CCardBody>
              </CCard>
            </CCol>
          }
        </CRow>
      </CContainer>
    </div>
  )
}

export default ConfirmRegister
