import React, {lazy} from 'react'
import {Route, Switch} from "react-router-dom"
import PrivateRoute from "../containers/PrivateRoute"
import UserInfoPage from "./UserInfoPage";

const ConfirmRegister = lazy(() => import('./ConfirmRegister'))
const ProblemsPage = lazy(() => import('./ProblemsPage'))
const Auth = lazy(() => import('./auth/Auth'))
const LoginPage = lazy(() => import('./LoginPage'))
const RegisterPage = lazy(() => import('./RegisterPage'))
const Page404 = lazy(() => import('./Page404'))
const MainPage = lazy(() => import('./MainPage'))
const ProblemPage = lazy(() => import('./ProblemPage'))
const SubmissionPage = lazy(() => import('./SubmissionPage'))
const SubmissionsPage = lazy(() => import('./SubmissionsPage'))
const SubmitPage = lazy(() => import('./SubmitPage'))
const ProfilePage = lazy(() => import('./ProfilePage'))
const RankingPage = lazy(() => import('./RankingPage'))

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"/>
  </div>
)

function Routes() {
  return (
    <React.Suspense fallback={loading}>
      <Switch>
        <Route path="/auth" name="Auth" render={props => <Auth {...props} />} />
        <Route exact path="/login" name="Login" render={props => <LoginPage {...props} />} />
        <Route exact path="/register" name="Register" render={props => <RegisterPage {...props} />} />
        <Route exact path="/confirm_register/:token" name="ConfirmRegister" render={props => <ConfirmRegister {...props} />} />
        <Route exact path="/problems" name="Problems" render={props => <ProblemsPage {...props} />} />
        <Route exact path="/problems/:id" name="Problems/id" render={props => <ProblemPage {...props} />} />
        <Route exact path="/submission/:submissionId" name="Submission/id" render={props => <SubmissionPage {...props} />} />
        <Route exact path="/submissions/submit/:id" name="Submit" render={props => <SubmitPage {...props} />} />
        <Route exact path="/submissions/submit" name="Submit" render={props => <SubmitPage {...props} />} />
        <Route exact path="/submissions" name="Submit" render={props => <SubmissionsPage {...props} />} />
        <PrivateRoute exact path="/profile" name="Profile">
          <ProfilePage/>
        </PrivateRoute>
        <PrivateRoute exact path="/users/:userId" name="UserInfo">
          <UserInfoPage/>
        </PrivateRoute>
        <PrivateRoute exact path="/ranking" name="Ranking">
          <RankingPage/>
        </PrivateRoute>
        <Route exact path="/" name="Main" render={props => <MainPage {...props} />} />
        <Route exact path="/404" name="Page 404" render={props => <Page404 {...props}/>} />
        <Route name="nowhere" render={props => <Page404 {...props} />} />
      </Switch>
    </React.Suspense>
  )
}

export default Routes
