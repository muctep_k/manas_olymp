import React, { useCallback, useContext, useState } from 'react'
import ManasServiceContext from "../services/manasServiceContext"
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow,
  CSpinner
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import {useTranslation} from "react-i18next";

const nullErrors = {
  firstname: null,
  lastname: null,
  email: null,
  password: null,
  confirmPassword: null,
  network: null,
  hasErrors: false
}

const RegisterPage = () => {

  const manasService = useContext(ManasServiceContext)
  const {t, i18n} = useTranslation('common')

  const [firstname, setFirstname] = useState('')
  const [lastname, setLastname] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')
  const [errors, setErrors] = useState(nullErrors)
  const [hasVerificationSent, setHasVerificationSent] = useState(false)
  const [isLoading, setIsLoading] = useState(false)

  const firstnameChangeHandler = useCallback(event => {
    setFirstname(event.target.value)
    setErrors(state => ({...state, firstname: null}))
  }, [])

  const lastnameChangeHandler = useCallback(event => {
    setLastname(event.target.value)
    setErrors(state => ({...state, lastname: null}))
  }, [])

  const emailChangeHandler = useCallback(event => {
    setEmail(event.target.value);
    setErrors(state => ({...state, email: null}))
  }, [])

  const passwordChangeHandler = useCallback(event => {
    setPassword(event.target.value)
    setErrors(state => ({...state, password: null}))
  }, [])

  const confirmPasswordChangeHandler = useCallback(event => {
    setConfirmPassword(event.target.value)
    setErrors(state => ({...state, confirmPassword: null}))
  }, [])

  const submitHandler = useCallback(async event => {
    event.preventDefault()
    setIsLoading(true)
    setErrors(nullErrors)

    const form = { firstname, lastname, email, password, confirmPassword }

    const result = await manasService.register(form)

    const {data, success} = result

    if (success){
      setHasVerificationSent(true)
      setIsLoading(false)
    } else {
      const err = {
        firstname: (data && data.detail && data.detail.first_name && data.detail.first_name[0]) || null,
        lastname: (data && data.detail && data.detail.last_name && data.detail.last_name[0]) || null,
        email: (data && data.detail && data.detail.email && data.detail.email[0]) || null,
        password: (data && data.detail && data.detail.password && data.detail.password[0]) || null,
        confirmPassword: (data && data.detail && data.detail.confirm_password && data.detail.confirm_password[0]) || null,
        network: (data && data.message) || null,
        hasErrors: true
      }
      setErrors(err)
      setIsLoading(false)
    }

  }, [firstname, lastname, email, password, confirmPassword])

  return (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="9" lg="7" xl="6">
            <CCard className="mx-4">
              {hasVerificationSent ?
                <CCardBody className="p-4">
                  <h3>{t('registerPage.sentVerificationTitle')}</h3>
                  <h5 className="mt-3">{t('registerPage.sentVerificationContent')}</h5>
                </CCardBody> :
                <>
                  <CCardBody className="p-4">
                    <CForm onSubmit={submitHandler}>
                      <h1>{t('registerPage.register')}</h1>
                      <p className="text-muted">{t('registerPage.createYourAccount')}</p>
                      <CInputGroup className="mt-3">
                        <CInputGroupPrepend>
                          <CInputGroupText>
                            <CIcon name="cil-user" />
                          </CInputGroupText>
                        </CInputGroupPrepend>
                        <CInput
                          type="text"
                          placeholder={t('registerPage.firstname')}
                          autoComplete="firstname"
                          value={firstname}
                          onChange={firstnameChangeHandler}
                        />
                      </CInputGroup>
                      { errors.firstname && <p style={{ color: 'red' }}>{ errors.firstname }</p> }
                      <CInputGroup className="mt-3">
                        <CInputGroupPrepend>
                          <CInputGroupText>
                            <CIcon name="cil-user" />
                          </CInputGroupText>
                        </CInputGroupPrepend>
                        <CInput
                          type="text"
                          placeholder={t('registerPage.lastname')}
                          autoComplete="lastname"
                          value={lastname}
                          onChange={lastnameChangeHandler}
                        />
                      </CInputGroup>
                      { errors.lastname && <p style={{ color: 'red' }}>{ errors.lastname }</p> }
                      <CInputGroup className="mt-3">
                        <CInputGroupPrepend>
                          <CInputGroupText>@</CInputGroupText>
                        </CInputGroupPrepend>
                        <CInput
                          type="text"
                          placeholder="Email"
                          autoComplete="email"
                          value={email}
                          onChange={emailChangeHandler}
                        />
                      </CInputGroup>
                      { errors.email && <p style={{ color: 'red' }}>{ errors.email }</p> }
                      <CInputGroup className="mt-3">
                        <CInputGroupPrepend>
                          <CInputGroupText>
                            <CIcon name="cil-lock-locked" />
                          </CInputGroupText>
                        </CInputGroupPrepend>
                        <CInput
                          type="password"
                          placeholder={t('registerPage.password')}
                          autoComplete="new-password"
                          value={password}
                          onChange={passwordChangeHandler}
                        />
                      </CInputGroup>
                      { errors.password && <p style={{ color: 'red' }}>{ errors.password }</p> }
                      <CInputGroup className="mt-4">
                        <CInputGroupPrepend>
                          <CInputGroupText>
                            <CIcon name="cil-lock-locked" />
                          </CInputGroupText>
                        </CInputGroupPrepend>
                        <CInput
                          type="password"
                          placeholder={t('registerPage.repeatPassword')}
                          autoComplete="new-password"
                          value={confirmPassword}
                          onChange={confirmPasswordChangeHandler}
                        />
                      </CInputGroup>
                      { errors.confirmPassword && <p style={{ color: 'red' }}>{ errors.confirmPassword }</p> }
                      { isLoading ? <div className="d-flex justify-content-center"><CSpinner className="mt-3" color="info" /></div> :
                        <CButton className="mt-3" type="submit" color="success" block>{t('registerPage.createAccount')}</CButton>
                      }
                    </CForm>
                  </CCardBody>
                  <CCardFooter className="p-4">
                  <CRow>
                    <CCol xs="12">
                      { (errors.network && <h6 className="border-danger p-3">{errors.network}</h6> )}
                    </CCol>
                  </CRow>
                </CCardFooter>
                </>}
            </CCard>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}

export default RegisterPage
