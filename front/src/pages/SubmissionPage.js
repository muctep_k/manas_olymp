import React from 'react'
import {TheFooter, TheHeader, TheSidebar} from "../containers"
import SubmissionContent from "../components/submission/SubmissionContent"

function SubmissionPage(props) {

  return (
    <div className="c-app c-default-layout">
      <TheSidebar/>
      <div className="c-wrapper">
        <TheHeader/>
        <div className="c-body">
          <SubmissionContent/>
        </div>
        <TheFooter/>
      </div>
    </div>
  )
}

export default SubmissionPage
