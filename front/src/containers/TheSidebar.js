import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  CCreateElement,
  CSidebar,
  CSidebarBrand,
  CSidebarNav,
  CSidebarNavDivider,
  CSidebarNavTitle,
  CSidebarMinimizer,
  CSidebarNavDropdown,
  CSidebarNavItem,
  CImg
} from '@coreui/react'
import ManasLogo from '../assets/images/Manas_Logo.png'

import CIcon from '@coreui/icons-react'

// sidebar nav config
import navigation from './_nav'
import {set} from "../redux/actions/sidebarActions";
import {useTranslation} from "react-i18next";

const TheSidebar = () => {
  const dispatch = useDispatch()
  const show = useSelector(state => state.sidebar.sidebarShow)

  const {t} = useTranslation('common')

  const _nav =  [
    {
      _tag: 'CSidebarNavItem',
      name: t('header.main'),
      to: '/',
      icon: <CIcon name="cil-home" customClasses="c-sidebar-nav-icon"/>
    },
    {
      _tag: 'CSidebarNavItem',
      name: t('header.problemList'),
      to: '/problems',
      icon: <CIcon name="cil-list" customClasses="c-sidebar-nav-icon"/>
    },
    {
      _tag: 'CSidebarNavItem',
      name: t('header.sendSubmission'),
      to: '/submission',
      icon: <CIcon name="cil-list" customClasses="c-sidebar-nav-icon"/>
    },
    {
      _tag: 'CSidebarNavItem',
      name: t('header.ranking'),
      to: '/ranking',
      icon: <CIcon name="cil-list" customClasses="c-sidebar-nav-icon"/>
    }
  ]

  return (
    <CSidebar
      show={show}
      onShowChange={(val) => dispatch(set(val))}
    >
      <CSidebarBrand className="d-md-down-none" to="/">
        <div className="c-sidebar-brand-full">
          <div className="d-flex justify-content-around">
            <CImg
              src={ManasLogo}
              height="50"
            />
            <div style={{ fontSize: 23, padding: 10 }}>Olymp KTMU</div>
          </div>
        </div>
        {/*<CIcon*/}
        {/*  className="c-sidebar-brand-minimized"*/}
        {/*  name="sygnet"*/}
        {/*  height={35}*/}
        {/*/>*/}
        <CImg
          src={ManasLogo}
          height="50"
          className="c-sidebar-brand-minimized"
        />
      </CSidebarBrand>
      <CSidebarNav>

        <CCreateElement
          items={navigation}
          components={{
            CSidebarNavDivider,
            CSidebarNavDropdown,
            CSidebarNavItem,
            CSidebarNavTitle
          }}
        />
      </CSidebarNav>
      <CSidebarMinimizer className="c-d-md-down-none"/>
    </CSidebar>
  )
}

export default React.memo(TheSidebar)
