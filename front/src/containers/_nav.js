import React from 'react'
import CIcon from '@coreui/icons-react'

const _nav =  [
  {
    _tag: 'CSidebarNavItem',
    name: 'Главная',
    to: '/',
    icon: <CIcon name="cil-home" customClasses="c-sidebar-nav-icon"/>
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Задачи',
    to: '/problems',
    icon: <CIcon name="cil-list" customClasses="c-sidebar-nav-icon"/>
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Рейтинг',
    to: '/ranking',
    icon: <CIcon name="cil-list" customClasses="c-sidebar-nav-icon"/>
  }
]

export default _nav
