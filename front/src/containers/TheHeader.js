import React, {useCallback} from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  CHeader,
  CToggler,
  CHeaderBrand,
  CHeaderNav,
  CHeaderNavItem,
  CHeaderNavLink,
  CImg,
  CButton
} from '@coreui/react'

import {
  TheHeaderDropdown,
}  from './index'
import {set} from "../redux/actions/sidebarActions"
import ManasLogo from "../assets/images/Manas_Logo.png"
import {useHistory} from "react-router-dom";
import {useTranslation} from "react-i18next";

const TheHeader = () => {
  const dispatch = useDispatch()
  const history = useHistory()
  const {t, i18n} = useTranslation('common')

  const sidebarShow = useSelector(state => state.sidebar.sidebarShow)
  const isAuthenticated = useSelector(state => state.auth.isAuthenticated)

  const toggleSidebar = () => {
    const val = [true, 'responsive'].includes(sidebarShow) ? false : 'responsive'
    dispatch(set(val))
  }

  const toggleSidebarMobile = () => {
    const val = [false, 'responsive'].includes(sidebarShow) ? true : 'responsive'
    dispatch(set(val))
  }

  const onLoginButtonClick = () => history.push('/login')
  const onRegisterButtonClick = () => history.push('/register')

  const onLangChange = useCallback((e, lang) => {
    e.preventDefault()
    i18n.changeLanguage(lang).then(() => {})
    localStorage.setItem("lang_manas_olymp", lang)
  }, [i18n])

  return (
    <CHeader withSubheader>
      <CToggler
        inHeader
        className="ml-md-3 d-lg-none"
        onClick={toggleSidebarMobile}
      />
      <CToggler
        inHeader
        className="ml-3 d-md-down-none"
        onClick={toggleSidebar}
      />
      <CHeaderBrand className="mx-auto d-lg-none" to="/">
        {/*<CIcon name="logo" height="48" alt="Logo"/>*/}
        <div className="d-flex justify-content-around">
          <CImg
            src={ManasLogo}
            height="50"
          />
          <div style={{ fontSize: 23, padding: 10 }}>Olymp KTMU</div>
        </div>
      </CHeaderBrand>

      <CHeaderNav className="d-md-down-none mr-auto">
        <CHeaderNavItem className="px-3" >
          <CHeaderNavLink to="/">{ t('header.main') }</CHeaderNavLink>
        </CHeaderNavItem>
        <CHeaderNavItem  className="px-3">
          <CHeaderNavLink to="/problems">{ t('header.problemList') }</CHeaderNavLink>
        </CHeaderNavItem>
        <CHeaderNavItem className="px-3">
          <CHeaderNavLink to="/submissions/submit">{ t('header.sendSubmission') }</CHeaderNavLink>
        </CHeaderNavItem>
        <CHeaderNavItem className="px-3">
          <CHeaderNavLink to="/ranking">{ t('header.ranking') }</CHeaderNavLink>
        </CHeaderNavItem>
      </CHeaderNav>

      <CHeaderNav>
        <a className={`m-1 cursor-pointer ${i18n.language === 'en' ? 'text-success' : ''}`}
           onClick={e => onLangChange(e, 'en')}>
          EN
        </a> |
        <a className={`m-1 cursor-pointer ${i18n.language === 'ru' ? 'text-success' : ''}`}
           onClick={e => onLangChange(e, 'ru')}>
          RU
        </a> |
        <a className={`m-1 cursor-pointer ${i18n.language === 'ky' ? 'text-success' : ''}`}
           onClick={e => onLangChange(e, 'ky')}>
          KY
        </a>
      </CHeaderNav>

      <CHeaderNav className="px-3">
        {
          isAuthenticated ? <>
              <TheHeaderDropdown/>
            </> :
          <div className="d-flex justify-content-between">
            <CButton color="primary" onClick={onLoginButtonClick}>{t('header.login')}</CButton>
            <CButton color="secondary" className="ml-4" onClick={onRegisterButtonClick}>{t('header.register')}</CButton>
          </div>
        }
      </CHeaderNav>
    </CHeader>
  )
}

export default TheHeader
