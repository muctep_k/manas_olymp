import React from 'react'
import { CFooter } from '@coreui/react'

const TheFooter = () => {
  return (
    <CFooter fixed={false}>
      <div>
        <a href="/" target="_blank" rel="noopener noreferrer">Olymp</a>
        <span className="ml-1">&copy; 2021 Kyrgyz-Turkish Manas University.</span>
      </div>
      <div className="mfs-auto">
        <span className="mr-1">Powered by</span>
        <a href="https://ru.reactjs.org/" target="_blank" rel="noopener noreferrer">Create React App</a>
      </div>
    </CFooter>
  )
}

export default React.memo(TheFooter)
