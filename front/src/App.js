import React, { useEffect } from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import './scss/style.scss'
import Routes from "./pages/Routes"
import {useDispatch, useSelector} from "react-redux"
import {loadUser} from "./redux/actions/authActions"
import FullPageSpinner from "./components/spinners/FullPageSpinner"
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

const App = () => {

  const dispatch = useDispatch()
  const isUserLoading = useSelector(state => state.auth.isUserLoading)

  useEffect(() => dispatch(loadUser()), [])

  return (
    <>
      <Router>
        { isUserLoading ? <FullPageSpinner/> : <Routes/> }
      </Router>
      <ToastContainer/>
    </>
  )
}

export default App
