import 'react-app-polyfill/ie11' // For IE 11 support
import 'react-app-polyfill/stable'
import 'core-js'
import './polyfill'
import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import * as serviceWorker from './serviceWorker'

import i18next from "i18next"
import {I18nextProvider} from "react-i18next";

import common_en from "./translations/en/common.json"
import common_ru from "./translations/ru/common.json"
import common_ky from "./translations/ky/common.json"


import { icons } from './assets/icons'

import { Provider } from 'react-redux'
import store from './redux/store'

import ManasService from "./services/manasService"
import ManasServiceContext from "./services/manasServiceContext"

React.icons = icons

const langFromStorage = localStorage.getItem('lang_manas_olymp')

const lng = langFromStorage || 'ru'

i18next.init({
  interpolation: {escapeValue: false},  // React already does escaping
  lng,                            // language to use
  resources: {
    en: {
      common: common_en               // 'common' is our custom namespace
    },
    ru: {
      common: common_ru               // 'common' is our custom namespace
    },
    ky: {
      common: common_ky
    },
  },
}).then(r => {})


ReactDOM.render(
  <Provider store={store}>
    <ManasServiceContext.Provider value={new ManasService()}>
      <I18nextProvider i18n={i18next}>
        <App/>
      </I18nextProvider>
    </ManasServiceContext.Provider>
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
