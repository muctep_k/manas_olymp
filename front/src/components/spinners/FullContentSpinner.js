import React from 'react'
import { CSpinner } from '@coreui/react'

function FullContentSpinner(props) {
  return (
    <div className="d-flex justify-content-center align-items-center" style={{ height: '90vh' }}>
      <CSpinner color="info" />
    </div>
  )
}

export default FullContentSpinner
