import React, {useEffect, useState, useContext, useCallback} from 'react'
import ProblemsTable from "./ProblemsTable"
import {
  CPagination,
  CSpinner,
  CCol,
  CRow,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CFormGroup,
  CInput,
  CLabel
} from "@coreui/react"
import ManasServiceContext from "../../services/manasServiceContext"
import Select from "react-select"
import {useTranslation} from "react-i18next"

function ProblemsContent(props) {

  const {t, i18n} = useTranslation('common')

  const [pageCount, setPageCount] = useState(1)

  const [currentPage, setCurrentPage] = useState(1)
  const [problems, setProblems] = useState([])
  const [isLoading, setIsLoading] = useState(false)

  const [searchText, setSearchText] = useState('')
  const [tags, setTags] = useState([{ value: null, label: 'Все' }])
  const [selectedTag, setSelectedTag] = useState({ value: null, label: 'Все' })

  const complexities = [
    { value: null, label: t('problemList.all') },
    { value: 'very_easy', label: t('problemList.veryEasy') },
    { value: 'easy', label: t('problemList.easy') },
    { value: 'medium', label: t('problemList.medium') },
    { value: 'difficult', label: t('problemList.difficult') },
    { value: 'very_difficult', label: t('problemList.veryDifficult') },
  ]
  const [selectedComplexity, setSelectedComplexity] = useState( { value: null, label: t('problemList.all') } )

  const manasService = useContext(ManasServiceContext)

  const fetchProblems = useCallback(async () => {
    setIsLoading(true)

    let params = `?page=${currentPage}`

    if (selectedTag.value){
      params += `&tag_id=${selectedTag.value}`
    }

    if (selectedComplexity.value){
      params += `&complexity=${selectedComplexity.value}`
    }

    if (searchText){
      params += `&search=${searchText}`
    }

    const { data, success } = await manasService.fetchProblems(params)

    if (success){
      setProblems(data.results)
      setPageCount(Math.round(data.count / 50 + 1))
    } else {

    }

    setIsLoading(false)
  }, [currentPage, selectedComplexity, selectedTag, searchText])

  const fetchTags = useCallback(async () => {
    const { success, data } = await manasService.getTags()
    success && setTags([{ value: null, label: t('problemList.all') }, ...data.map(tag => ({ value: tag.id, label: tag.title }))])
  }, [])

  useEffect(() => {

    const str = t('problemList.all')

    setSelectedTag({value: null, label: str })
    setSelectedComplexity({value: null, label: str })
  }, [i18n.language])

  useEffect(() => { fetchTags().then(() => {}) }, [])

  useEffect(() => {
    window.scrollTo({top: 0, behavior: "smooth"})
    fetchProblems(currentPage).then(() => {})
  }, [currentPage, searchText, selectedTag, selectedComplexity])

  return (
    <div className="p-4 container-sm" >
      <CCard>
        <CCardHeader>
          <CRow>
            {/*<CCol>*/}
            {/*  <CFormGroup>*/}
            {/*    <CLabel>*/}
            {/*      Поиск*/}
            {/*    </CLabel>*/}
            {/*    <CInput*/}
            {/*      value={searchText}*/}
            {/*      onChange={e => setSearchText(e.target.value)}*/}
            {/*      placeholder="Введите текст для поиска..."*/}
            {/*    />*/}
            {/*  </CFormGroup>*/}
            {/*</CCol>*/}
            <CCol>
              <CFormGroup>
                <CLabel>
                  {t('problemList.tags')}
                </CLabel>
                <Select
                  value={selectedTag}
                  options={tags}
                  onChange={val => setSelectedTag(val)}
                />
              </CFormGroup>
            </CCol>
            <CCol>
              <CFormGroup>
                <CLabel>
                  {t('problemList.complexity')}
                </CLabel>
                <Select
                  value={selectedComplexity}
                  options={complexities}
                  onChange={val => setSelectedComplexity(val)}
                />
              </CFormGroup>
            </CCol>
          </CRow>
        </CCardHeader>
        <CCardBody>
          { isLoading ?
            <CRow>
              <CCol className="d-flex justify-content-center align-items-center min-vh-100">
                <CSpinner color="info" size="lg" />
              </CCol>
            </CRow>
            : <ProblemsTable problems={problems}/> }
        </CCardBody>
        <CCardFooter>
          <div className={'mt-2'} >
            <CPagination
              activePage={currentPage}
              pages={pageCount}
              onActivePageChange={(i) => setCurrentPage(i)}
            />
          </div>
        </CCardFooter>
      </CCard>
    </div>
  )
}

export default ProblemsContent
