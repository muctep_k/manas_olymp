import React, {useCallback} from 'react'
import {CDataTable, CBadge, CCard, CCardBody } from "@coreui/react"
import {useHistory} from "react-router-dom"
import {useTranslation} from "react-i18next";

const ProblemsTable = ({ problems }) => {

  const history = useHistory()
  const { t, i18n } = useTranslation('common')

  const fields = [
    {
      key: 'title',
      label: t('problemList.title'),
      _style: { width: '25%' },
      filter: false
    },
    {
      key: 'all_submissions',
      label: t('problemList.allSubmissions'),
      filter: false
    },
    {
      key: 'successful_submissions',
      label: t('problemList.successfulSubmissions'),
      filter: false
    },
    {
      key: 'tags',
      label: t('problemList.tags'),
      filter: false
    },
    {
      key: 'complexity',
      label: t('problemList.complexity'),
      filter: false
    },
    {
      key: 'status',
      label: t('problemList.status'),
      filter: false
    }
  ]

  const getComplexityWithBadge = useCallback(complexity => {
    switch (complexity){
      case 'Difficult':
        return <CBadge color='warning'>{ t('problemList.difficult') }</CBadge>
      case 'Easy':
        return <CBadge color='secondary'>{ t('problemList.easy') }</CBadge>
      case 'Medium':
        return <CBadge color='info'>{ t('problemList.medium') }</CBadge>
      case 'Very Easy':
        return <CBadge color='success'>{ t('problemList.veryEasy') }</CBadge>
      case 'Very Difficult':
        return <CBadge color='danger'>{ t('problemList.veryDifficult') }</CBadge>
      default: return ''
    }
  }, [i18n])

  return (
    <CDataTable
      items={problems}
      fields={fields}
      columnFilter
      footer
      hover
      sorter
      clickableRows
      onRowClick={item => history.push(`/problems/${item.id}`)}
      scopedSlots = {{
        'title':
          (item)=>(
            <td>
              { item.title }
            </td>
          ),
        'all_submissions':
          (item)=>(
            <td>
              { ( 100 - parseInt(item.successful_submissions) * (100 / item.all_submissions) ).toFixed(1) } %
            </td>
          ),
        'successful_submissions':
          (item)=>(
            <td>
              { item.successful_submissions }
            </td>
          ),
        'complexity':
          (item)=>(
            <td>
              {getComplexityWithBadge(item.complexity)}
            </td>
          ),
        'tags':
          (item)=>(
            <td>
              { item.tags.map(el => (el + ', ')) }
            </td>
          ),
        'status':
          (item)=>(
            <td>
              { item.status }
            </td>
          ),
      }}
    />
  )
}

export default ProblemsTable
