import React, {useState, useCallback, useEffect, useContext} from 'react'
import {useHistory, useParams} from "react-router-dom"
import ManasServiceContext from "../../services/manasServiceContext"
import {
  CButton,
  CCol,
  CRow,
  CTabs,
  CNav,
  CNavLink,
  CNavItem,
  CTabContent,
  CTabPane,
  CCard,
  CCardBody,
  CCardHeader,
  CLabel,
  CLink,
  CDataTable,
  CTextarea,
  CFormGroup,
  CInput
} from "@coreui/react"
import { PieChart } from "react-minimal-pie-chart"
import FullPageSpinner from "../spinners/FullPageSpinner"
import {useTranslation} from "react-i18next";

function SubmissionContent(props) {

  const manasService = useContext(ManasServiceContext)
  const { submissionId } = useParams()
  const history = useHistory()
  const {t} = useTranslation('common')

  const [isSubmissionLoading, setIsSubmissionLoading] = useState(false)
  const [submissionResult, setSubmissionResult] = useState(null)

  const getSubmission = useCallback(async id => {
    if (id){
      const { success, data } = await manasService.getSubmission(id)

      console.log('submission success: ', success)
      console.log('submission data: ', data)

      if (success){
        setSubmissionResult(data)
      } else {

      }

    }

    setIsSubmissionLoading(false)
    return 'success'
  }, [submissionId])

  const refresh = useCallback(() => {
    setIsSubmissionLoading(true)
    getSubmission(submissionId)
  }, [submissionId])

  useEffect(() => {
    setIsSubmissionLoading(true)
    getSubmission(submissionId).then(r => {})

    const timeout = setTimeout(() => {
      getSubmission(submissionId)
    }, 4000)

    const timeout2 = setTimeout(() => {
      getSubmission(submissionId)
    }, 8000)

    return () => {
      clearTimeout(timeout)
      clearTimeout(timeout2)
    }
  }, [])

  const fields = [
    { key: 'name', label: t('submission.test') },
    { key: 'status', label: t('submission.status') },
    { key: 'time_taken', label: t('submission.time') },
    { key: 'memory_taken', label: t('submission.memory') },
  ]

  return (
    <div>
      <CRow>
        <CCol md={2}/>
        <CCol md={8}>
          <CRow>
            <CCol><h2 className="my-3">{t('submission.submission')} #{submissionId}</h2></CCol>
            <CCol>
              <div className="float-right mt-4">
                <CButton color="success" onClick={refresh}>{t('submission.refresh')}</CButton>
                <CButton className="ml-4" color="info" onClick={() => history.push(`/submission/${submissionResult.problem.id}`)}>
                  {t('submission.sendAgain')}
                </CButton>
              </div>
            </CCol>
          </CRow>
          <CTabs activeTab="results">
            <CNav variant="tabs">
              <CNavItem>
                <CNavLink data-tab="results">
                  {t('submission.results')}
                </CNavLink>
              </CNavItem>
              <CNavItem>
                <CNavLink data-tab="code">
                  {t('submission.sourceCode')}
                </CNavLink>
              </CNavItem>
            </CNav>
            <CTabContent>
              { isSubmissionLoading ? <FullPageSpinner/> : submissionResult && <>
                <CTabPane data-tab="results">
                  { isSubmissionLoading ? <FullPageSpinner/> :
                    <CCard className="mt-3">
                      <CCardHeader>
                        <CRow>
                          <CCol>
                            <h6 className="font-weight-bold">{t('submission.problem')}</h6>
                            <CLink to={`/problem/${submissionResult.problem.id}`}>{ submissionResult.problem.title }</CLink>
                          </CCol>
                          <CCol>
                            <h6 className="font-weight-bold">{t('submission.sent')}</h6>
                            <h6>{ submissionResult.submitted_at }</h6>
                          </CCol>
                          <CCol>
                            <h6 className="font-weight-bold">{t('submission.programmingLanguage')}</h6>
                            <h6>{ submissionResult.language }</h6>
                          </CCol>
                          <CCol>
                            <h6 className="font-weight-bold">{t('submission.author')}</h6>
                            <h6><CLink>{ submissionResult.user.first_name + ' ' + submissionResult.user.last_name }</CLink></h6>
                          </CCol>
                        </CRow>
                      </CCardHeader>
                      <CCardBody>
                        <CRow>
                          <CCol>
                            <CFormGroup className="px-3">
                              <CLabel>{t('submission.status')}</CLabel>
                              <CInput defaultValue={submissionResult.status} value={submissionResult.status} disabled/>
                            </CFormGroup>
                          </CCol>
                          <CCol>
                            <CFormGroup className="px-3">
                              <CLabel>{t('submission.time')} (s)</CLabel>
                              <CInput defaultValue={submissionResult.time_taken} value={submissionResult.time_taken} disabled/>
                            </CFormGroup>
                          </CCol>
                          <CCol>
                            <CFormGroup className="px-3">
                              <CLabel>{t('submission.memory')} (kb)</CLabel>
                              <CInput defaultValue={submissionResult.memory_taken} value={submissionResult.memory_taken} disabled/>
                            </CFormGroup>
                          </CCol>
                        </CRow>
                        <CRow>
                          <CCol className="p-4">
                            <PieChart
                              data={[{ value: getAcceptedTestCount(submissionResult.test_cases), color: '#28a745' }]}
                              totalValue={submissionResult.test_cases.length}
                              lineWidth={10}
                              label={({ dataEntry }) => (((getAcceptedTestCount(submissionResult.test_cases) * 100) / submissionResult.test_cases.length).toFixed(1) + '%')}
                              labelStyle={{
                                fontSize: '16px',
                                fontFamily: 'sans-serif',
                                fill: '#28a745',
                              }}
                              labelPosition={0}
                              background={'#EEEEEE'}
                            />
                          </CCol>
                          <CCol className="p-4">
                            <PieChart
                              data={[{ value: (submissionResult.time_taken * 1000), color: '#ffc107' }]}
                              totalValue={submissionResult?.problem?.time_limit * 1000}
                              lineWidth={10}
                              label={({ dataEntry }) => dataEntry.value + ' ms'}
                              labelStyle={{
                                fontSize: '16px',
                                fontFamily: 'sans-serif',
                                fill: '#ffc107',
                              }}
                              labelPosition={0}
                              background={'#EEEEEE'}
                            />
                          </CCol>
                          <CCol className="p-4">
                            <PieChart
                              data={[{ value: (submissionResult.memory_taken), color: '#17a2b8' }]}
                              totalValue={submissionResult?.problem?.memory_limit}
                              lineWidth={10}
                              label={({ dataEntry }) => dataEntry.value + ' KiB'}
                              labelStyle={{
                                fontSize: '16px',
                                fontFamily: 'sans-serif',
                                fill: '#17a2b8',
                              }}
                              labelPosition={0}
                              background={'#EEEEEE'}
                            />
                          </CCol>
                        </CRow>
                        <CRow>
                          <CCol>
                            <CDataTable
                              items={submissionResult.test_cases}
                              fields={fields}
                              hover
                              scopedSlots={{
                                'name':
                                  (item, idx) => (
                                    <td> { getStatusColor(item.status, `${t('submission.test')} #${idx + 1}`) } </td>
                                  )
                              }}
                            />
                          </CCol>
                        </CRow>
                      </CCardBody>
                    </CCard>
                  }
                </CTabPane>
                <CTabPane data-tab="code">
                  { isSubmissionLoading ? <FullPageSpinner/> :
                    <CRow>
                      <CCol>
                        <CCard>
                          <CCardBody>
                            <CTextarea spellCheck={false} rows={25} defaultValue={ submissionResult.code } disabled className="bg-white"/>
                          </CCardBody>
                        </CCard>
                      </CCol>
                    </CRow> }
                </CTabPane>
              </>
              }
            </CTabContent>
          </CTabs>
        </CCol>
        <CCol md={2}/>
      </CRow>
    </div>
  )
}

const getAcceptedTestCount = result => result.filter(test => test.status === 'Accepted').length

const getStatusColor = (status, number) => {
  switch (status){
    case 'Accepted':
      return <span className="text-success">{ number }</span>
    case 'Wrong Answer':
      return <span className="text-danger">{ number }</span>
    case 'Processing':
      return <span className="text-info">{ number }</span>
    case 'Compilation Error':
      return <span className="text-danger">{ number }</span>
    case 'Time Limit Exceeded':
      return <span className="text-danger">{ number }</span>
    default:
      return <span>{ status }</span>
  }
}

export default SubmissionContent
