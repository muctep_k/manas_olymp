import React, {useState, useEffect, useContext, useCallback} from 'react'
import {CButton, CCol, CFormGroup, CLabel, CSelect, CTextarea, CRow, CInput, CForm, CBadge, CCard, CCardBody, CContainer} from "@coreui/react"
import {useHistory, useParams} from "react-router-dom"
import ManasServiceContext from "../../services/manasServiceContext"
import FullPageSpinner from "../spinners/FullPageSpinner"

import { CodeEditorEditable } from 'react-code-editor-editable'
import 'highlight.js/styles/dracula.css'
import {useSelector} from "react-redux"
import {useTranslation} from "react-i18next";

function SubmitContent() {

  const history = useHistory()

  const { id } = useParams()
  const manasService = useContext(ManasServiceContext)
  const isAuth = useSelector(state => state.auth.isAuthenticated)
  const {t} = useTranslation('common')

  const [problemId, setProblemId] = useState(id || '')
  const [code, setCode] = useState('')
  const [langs, setLangs] = useState([])
  const [selectedLangId, setSelectedLangId] = useState('')
  const [error, setError] = useState(null)
  const [inputErrors, setInputErrors] = useState({ problemId: null, code: null })
  const [isLoading, setIsLoading] = useState(false)

  useEffect(() => {
    const fetchLanguages = async () => {
      const { success, data } = await manasService.fetchLanguages()
      if (success){
        setLangs(data)
        setSelectedLangId(data.length && (data[1].id || data[0].id))
      } else {

      }
    }

    fetchLanguages().then(() => {})
  }, [])

  const problemIdChangeHandler = useCallback(event => {
    setProblemId(event.target.value)
    setInputErrors(value => ({...value, problemId: null}))
  }, [])

  const codeChangeHandler = useCallback(val => {
    setCode(val)
    setInputErrors(value => ({...value, code: null}))
  }, [])

  const onSubmit = useCallback(async event => {
    event.preventDefault()
    setIsLoading(true)

    if (!isAuth){
      history.push('/login')
      return
    }

    if (selectedLangId && code && problemId){
      const { success, data } = await manasService.createSubmission(parseInt(problemId), code, parseInt(selectedLangId))

      if (success){
        history.push(`/submission/${data.id}`)
      } else {
        setError((data?.detail?.object_error && data?.detail?.object_error[0]) || 'Что-то пошло не так!')
      }
    } else {
      !problemId.length && (setInputErrors(value => ({...value, problemId: 'Это поле не может быть пустым!'})))
      !code.length && (setInputErrors(value => ({...value, code: 'Это поле не может быть пустым!'})))
    }

    setIsLoading(false)
  }, [code, selectedLangId, problemId, isAuth])

  return (
    <CContainer className="custom-container-xs">
      <CCard className="mt-3">
        <CCardBody>
          <CForm onSubmit={onSubmit}>
            {
              isLoading ? <FullPageSpinner/> :
                <div>
                  <h3 className="my-3">{ t('submit.sendSubmission') }</h3>
                  { error && <CBadge color="danger" className="mb-3 font-lg px-3">{ error }</CBadge> }
                  <CFormGroup>
                    <CLabel htmlFor="problem-id">{ t('submit.problem') }</CLabel>
                    <CInput
                      id="problem-id"
                      placeholder={t('submit.enterProblemNumber')}
                      value={problemId}
                      type="number"
                      onChange={problemIdChangeHandler}
                    />
                    { inputErrors.problemId && <CBadge color="danger" className="font-lg mt-3">{ inputErrors.problemId }</CBadge> }
                  </CFormGroup>
                  <CFormGroup>
                    <CLabel htmlFor="select">{t('submit.language')}</CLabel>
                    <CSelect custom name="select" id="select" value={selectedLangId} onChange={event => setSelectedLangId(event.target.value)}>
                      { langs.map(l => <option key={l.id} value={l.id}>{l.name}</option>) }
                    </CSelect>
                  </CFormGroup>
                  <CFormGroup>
                    <CLabel htmlFor="code-textarea">{ t('submit.sourceCode') }</CLabel>
                    <CodeEditorEditable
                      value={code}
                      setValue={codeChangeHandler}
                      width='100%'
                      height='50vh'
                      language={getHighlightType(selectedLangId)}
                      inlineNumbers
                    />
                    { inputErrors.code && <CBadge color="danger" className="font-lg mt-3">{ inputErrors.code }</CBadge> }
                  </CFormGroup>
                  <CButton color="success" className="mt-4" type="submit">{ t('submit.sendSubmission') }</CButton>
                </div>
            }
          </CForm>
        </CCardBody>
      </CCard>
    </CContainer>
  )
}

const getHighlightType = id => {
  switch (id){
    case 50: case '50': return 'c'
    case 54: case '54': return 'cpp'
    case 51: case '51': return 'cs'
    case 60: case '60': return 'go'
    case 62: case '62': return 'java'
    case 63: case '63': return 'js'
    case 67: case '67': return 'pascal'
    case 68: case '68': return 'php'
    case 71: case '71': return 'py'
    default: return 'cpp'
  }
}

export default SubmitContent
