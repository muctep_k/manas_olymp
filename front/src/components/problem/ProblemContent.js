import React, { useCallback, useContext, useEffect, useState } from 'react'
import ManasServiceContext from "../../services/manasServiceContext"
import {useHistory, useParams} from "react-router-dom"
import FullPageSpinner from "../spinners/FullPageSpinner"
import { CRow, CCol, CCard, CCardBody, CButton, CContainer } from '@coreui/react'
import CIcon from "@coreui/icons-react"
import MathJax from 'simple-react-mathjax'
import {useTranslation} from "react-i18next";

function ProblemContent(props) {

  const manasService = useContext(ManasServiceContext)
  const { id } = useParams()
  const history = useHistory()
  const {t} = useTranslation('common')

  const [isProblemLoading, setIsProblemLoading] = useState(false)
  const [error, setError] = useState(null)
  const [problem, setProblem] = useState(null)

  const fetchProblem = useCallback(async i => {
    setIsProblemLoading(true)

    const { success, data } = await manasService.fetchProblemById(i)

    if (success){
      setError(null)
      setProblem(data)
    } else {
      setError(data.detail.toString())
    }

    setIsProblemLoading(false)
  }, [])

  useEffect(() => {
    fetchProblem(id).then(() => {})
  }, [id])

  const submissions = [
    { id: 1, status: 'accepted', language: 'c++', time_taken: 2000, memory_taken: 2000, submitted_at: '29.01.21' },
    { id: 2, status: 'time limit', language: 'python', time_taken: 2000, memory_taken: 2000, submitted_at: '29.01.21' },
    { id: 3, status: 'wrong answer', language: 'java', time_taken: 2000, memory_taken: 2000, submitted_at: '29.01.21' }
  ]

  console.log('problem: ', problem)

  return (
    <CContainer className="custom-container-xs">
      { isProblemLoading ? <FullPageSpinner/> :
        <>
          { error ? <div className="d-flex justify-content-center align-items-center h-100"> { error } </div> :
            <div>
              { problem &&
              <CCard className="mt-3">
                {/*<CCardHeader>*/}
                {/*  text*/}
                {/*</CCardHeader>*/}
                <CCardBody style={{ minHeight: '90vh' }}>
                  <div className="text-center font-2xl text-primary">{ problem.title }</div>
                  <div className="text-center font-sm text-info">{t('problem.timeLimit')}: { problem.time_limit } s</div>
                  <div className="text-center font-sm text-info">{t('problem.memoryLimit')}: { problem.memory_limit } mb</div>

                  <MathJax>
                    <div className="mt-3" dangerouslySetInnerHTML={{__html: problem.description}}/>
                  </MathJax>

                  <CRow className="mt-4">
                    <CCol xs={6}>
                      <span className="text-primary">{t('problem.inputExample')}</span>
                      <div className="border mt-1 p-2">
                        { problem.input_example }
                      </div>
                    </CCol>
                    <CCol xs={6}>
                      <span className="text-primary">{t('problem.outputExample')}</span>
                      <div className="border mt-1 p-2">
                        { problem.output_example }
                      </div>
                    </CCol>
                  </CRow>

                  <div className="mt-3 text-info mb-5">
                    <span className="text-primary">{t('problem.tags')}:</span> { problem.tags.map(t => <span key={t}>{ t }, </span>) }
                  </div>

                  <CButton color="warning" onClick={() => history.push(`/submissions/submit/${id}`)}>
                    {t('problem.sendSubmission')}
                    <CIcon className="ml-2" name="cil-arrow-right" />
                  </CButton>
                </CCardBody>
                <div className="mt-5 mx-3 mb-2">
                  <h4>Submissions</h4>
                  <table className="table">
                    <thead className="thead-dark">
                    <tr>
                      <th scope="col">ID</th>
                      <th scope="col">Status</th>
                      <th scope="col">Language</th>
                      <th scope="col">time taken</th>
                      <th scope="col">memory taken</th>
                      <th scope="col">submitted at</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                      submissions.map(s => (
                        <tr id={s.id}>
                          <th scope="row">{s.id}</th>
                          <td>{s.status}</td>
                          <td>{s.language}</td>
                          <td>{s.time_taken}</td>
                          <td>{s.memory_taken}</td>
                          <td>{s.submitted_at}</td>
                        </tr>
                      ))
                    }
                    </tbody>
                  </table>
                </div>
              </CCard> }
            </div>
          }
        </>
      }
    </CContainer>
  )
}

export default ProblemContent
