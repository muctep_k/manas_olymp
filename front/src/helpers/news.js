export const news = {
  status: "ok",
  totalResults: 1023,
  articles: [
    {
      source: {id: null, name: 'CNBC'},
      author: 'Abubakir Nunuly',
      title: `Спортивное программирование`,
      description: `В чем смысл участия на олимпиадах по программированию, почему они ценятся IT-компаниями и как правильно к ним готовиться?`,
      url: 'https://astanahub.com/blog/sportivnoe-programmirovanie1617782868',
      urlToImage: 'https://astanahub.com/media/nachalos-1024x768_0HhiT42.jpg-31bac5e90660-thumbnail-1500.png',
      published_at: 'April 7, 2021, 7:16 p.m.',
      content: `<div class="tiny-content">
            <p>Всем привет! В данном посте хотел бы затронуть тему олимпиадного программирования. Сам являюсь олимпиадником и готов поделиться с вами своими знаниями и опытом в этой сфере.</p>
            <p><strong style="font-style: inherit;">Что такое олимпиадное программирование и смысл участия на них</strong></p>
            <p>Олимпиада по программированию - интеллектуальное соревнование по решению различных задач, &nbsp;для решения которых необходимо придумать и применить какой-либо алгоритм или программу на одном из языков программирования. Олимпиады бывают личные и командные. Именно такие контесты являются отличной стартовой площадкой для карьеры в IT. Победители олимпиад часто становятся наставниками, готовят других чемпионов или устраиваются в крупные IT-компании. По некоторым данным, 5-10% общего числа сотрудников в крупных IT-компаниях - это бывшие участники крупнейших студенческих чемпионатов по программированию ICPC.</p>
            <p><strong style="font-style: inherit;">Почему участников олимпиад ценят в IT-компаниях</strong></p>
            <p>Во первых, олимпиадники пишут чистый, эффективный и быстродействующий код. Они хорошо оптимизируют код из-за знания алгоритмов, умеют быстро принимать решения и решать задачи разной сложности. Во вторых, олимпиадники умеют работать в команде, коллективе. Далеко не все умеют конструктивно совместно что-то обсуждать, совместно вырабатывать решения. Командные олимпиады учат этому. Учат слышать людей, принимать во внимание их сильные и слабые стороны, индивидуальные особенности. В третьих, участники олимпиад имеют гибкие навыки, а именно хороший тайм-менеджмент и стрессоустойчивость. Также, спортивное программирование учит людей думать, на контестах недостаточно знание определенного frameworkа или языка.</p>
            <p><strong style="font-style: inherit;">Как готовиться к контестам по спортивному программированию(по советам МФТИ)</strong></p>
            <p>С начала определите, достаточно ли у вас базовых знаний для участия в соревнованиях (математической подготовкой: умение решать нестандартные задачи и строить математические модели и знание алгоритмов). Следом, начните индивидуальную подготовку и изучайте больше теории. Почитайте справочник «Олимпиадное программирование», разберите материалы на бесплатном портале E-maxx - там собраны алгоритмы, электронные пособия и новости спортивного программирования. Самым главным, является практика. Постоянно решайте задачи ищите задания с прошлых чемпионатов и олимпиад. Их публикуют на Timus Online Judje, Codeforces, TopCoder, Spoj и CodeChef.</p>
            <p>&nbsp;</p>
            <p><strong style="font-style: inherit;">Спасибо за внимание и удачи!</strong></p>
            <p>&nbsp;</p>
          </div>`
    }
  ]
}
