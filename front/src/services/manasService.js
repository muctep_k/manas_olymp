// Olymp Manas Service

export default class ManasService {
  _baseApi = process.env.REACT_APP_BASE_API

  register = async (form) => {
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ form })
    }

    return await doRequestAndParse(`${_baseApi}/register/`, options)
  }

  confirmRegister = async token => {
    return await doRequestAndParse(`${_baseApi}/register/?token=${token}`)
  }

  login = async (email, password) => {
    let isError = false

    try {
      const res = await fetch(`${_baseApi}/api/login/`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ email, password })
      })

      if (!res.ok){
        isError = true
      }

      const data = await res.json()

      return { isError, data }
    } catch (e) {
      return { isError: true, message: e.message.toString() }
    }

  }

  fetchProblems = async params => await doRequestAndParse(`${this._baseApi}/problems/${params}`)

  fetchProblemById = async id => await doRequestAndParse(`${this._baseApi}/problem/${id}/`)

  fetchLanguages = async () => await doRequestAndParse(`${this._baseApi}/languages/`)

  createSubmission = async (id, code, language) => await doRequestAndParse(`${_baseApi}/create_submission/${id}/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${getToken()}`
      },
      body: JSON.stringify({ code, language })
    })

  getSubmission = async submissionId => {
    return await doRequestAndParse(`${this._baseApi}/submission/${submissionId}/`, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${getToken()}`
          }
        })
  }

  getSubmissions = async () => {
    return await doRequestAndParse(`${this._baseApi}/submissions/`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${getToken()}`
      }
    })
  }

  getRank = async (params = '') =>  await doRequestAndParse(`${_baseApi}/leaderboard/${params}`)

  getProfile = async (id = null) => {
    const options = {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${getToken()}`
      }
    }

    let param = ''

    if (id){
      param += `?user_id=${id}`
    }

    return await doRequestAndParse(`${_baseApi}/users/profile/${param}`, options)
  }

  updateProfile = async (formData) => {
    const options = {
      method: 'PATCH',
      headers: {
        'Authorization': `Bearer ${getToken()}`,
      },
      body: formData
    }

    return doRequestAndParse(`${_baseApi}/users/profile/`, options)
  }

  getTags = async () => doRequestAndParse(`${_baseApi}/tags/`)

}

export const doRequestAndParse = async (url, options = { method: 'GET' }) => {
  try {
    const response = await fetch(url, options)

    const data = await response.json()

    if (!response.ok){
      return { success: false, data }
    }

    console.log('response: ', response)
    console.log('data: ', data)

    return { success: true, data }

  } catch (e) {

    console.log('e: ', e)

    return { success: false, data: { detail: e.response?.data || e.message.toString() } }
  }
}

export const _baseApi = process.env.REACT_APP_BASE_API

const getToken = () => JSON.parse(localStorage.getItem('access_token_manas'))
